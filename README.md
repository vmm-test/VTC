<<<<<<< HEAD
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
=======
# VTC

VMM test card software

The VMM test card software consists of two parts, a python GUI that runs on a PC (folder gui), and a set of python scripts for the communication with the pyboard (folder pyboard). When the VTC is connected via USB to a PC, the pyboard appears as a flash drive. For the VTC to work, the scripts in the pyboard folder have to be copied into this flash drive.

The software is started from the folder src/gui by calling 

```
python gui.py

```
The preliminary documentation for the VMM testing is in the file Manual.pdf. The folder /src/console contains scripts that communicate directly with the pyboard via a terminal application like putty. In future it is foreseen to integrate these scripts into the GUI.

The VTC has been developed by Hans Muller (hans.muller@cern.ch). The software has been written by Hans Muller, Sorina Popescu (sorina.popescu@cern.ch) and Marek Hracek (marek.hracek@cern.ch).
>>>>>>> mhracek
