	# REV 2 29.06.2020  added P1 measurement at VMM hybrid 
	# REV 1 24.02.2020  added 4 sec boot delay after powerup, 
	# reversal of 2 lines after " Test and read  P2b voltage via ADC2" 
	# VMM hybrid Voltage and Current measurement for VTC board V1.0, 23.02.2020  H.Muller 
	# -measure P2a and P2b as VMM supply voltages and currents 
	# -calculate current P2 over current resistor R=0.22 OHM
	# -VMM3a hybrid connected via 2m HDMI cable + GND cable  between Hybrid and VTC box
	# -Voltages on VTC adjusted (trimmers R8 and R12:  P2 = 3.65, P1 = 3.6 V ) 
	# uPython v1.11 -580-g973f68780  on  PYBD-SF6W  
	# 
from machine import Pin
PWR_EN = Pin('X3', Pin.OUT) 	# define X3 (PA2)  as PWR_EN output
AMUX_EN = Pin('X1', Pin.OUT)	# define X1 (PA0)  as AMUX-EN output
GND_EN = Pin('X6', Pin.OUT) 	# define X6 (PA5) as GND enable (3V3) for HDMI pins 
CLK_EN = Pin('X4', Pin.OUT)		# define X4 (PA3) as Enable signal for 40 MHz VMM clock
	#	
CLK_EN.value(1)			# Enable 40 MHz clock
	#
GND_EN.value(1)			# Put all HDMI pins 2,5,11,17 to GND
	# 
AMUX_EN.value(1)			# enable analogue multiplexters for ADC  
	#
PWR_EN.value(1)			# enable power P1 and P2 for VMM (green LED)
 				# enable analogue multiplexers

	# ------------------------------------------
	# VMM boot delay after Powerup
	# ----------------------------------------
for i in range(4000000):	# let wait( 4M*1n=4s) to let VMM boot 
	pass	          
	# ------------------------------------------
	# Control Multiplexers via I2C bus No 1
	# ----------------------------------------
i2c = machine.I2C('X')		# set up I2C via X9 (SCL) and X10 (SCA)
from machine import Pin		# enable I2C  pullups inside MCU 
Pin('PULL_SDA', Pin.OUT, value=1)
Pin('PULL_SCL', Pin.OUT, value=1)
	# 
i2c.scan()   			#scan and find i2C address of REG chip ( PCA9534 )
				# should be [32]
addr=_[0]			# -[0] is address found (= 32)
	#
print('REG chip found at decimal addr=',addr)
i2c.writeto_mem(addr, 3, b'\x00') 	# write to REG chip CTRLregister 3: make all 8 outputs
	# -----------------------------------------
	#  Set up and test:  ADC 2 to read P2a voltage 
	# ------------------------------------------	
from pyb import Pin, ADC				
				# route pin y4 (  0.5*P2a ) of MUX3 to ADC2
i2c.writeto_mem(addr, 1, b'\x20') 	# set MUX3 address to y4 pin
adc2 = ADC(Pin('Y12')) 		# read ADC2 on PYBD  Y12 
	# -------------------------------------------
	#  Test read P2A  voltage via ADC2: 
	# -------------------------------------------
adc2.read()			# one ADC bit is 0.00080566 Volt
	# --------------------------------------------
CONV=0.00080566*2 		# conversion of ADC  bits in Volt	
CONV * adc2.read()		# in this example P2b =  3.36 V					
	# --------------------------------------------
	# Test and read  P2b voltage via ADC2: 
	# --------------------------------------------	
				# route pin y3  (0.5* P2B) of MUX1 to ADC1
i2c.writeto_mem(addr, 1, b'\x03') 	# set address to y3 pin
adc1 = ADC(Pin('X12')) 		# read ADC1 on pin X12 
	#---------------------------------------------
	# Calculate the P2 Current [A] and Voltages P2a ->P2b  over R2 (0.22 OHM)
	# --------------------------------------------
i2c.writeto_mem(addr, 1, b'\x20')  	# set MUX1 address  to 1/2 P2a
A = CONV * adc2.read()			# read MUX1 via ADC1
i2c.writeto_mem(addr, 1, b'\x03')  	# set MUX1 address  to 1/2 P2b
B=  CONV * adc1.read()			# read MUX1 via ADC1
I2 = ( A - B ) / 0.22			# Voltage  over R2 = 0.22 OHM
print('P2 current=',round(I2,2),'Ampere') # should be around 1.6A 
print('P2a=',round(A,2),'Volt')		# P2a should be around 3.6V
print('P2b=',round(B,2),'Volt')		# P2b should be around 3  Volt				
	#-----------------------------------------------
	# Calculate P1 Current [A] and Voltages P1a -> P1b over R1 (3.3 OHM)
	# ---------------------------------------------
i2c.writeto_mem(addr, 1, b'\x28')  	# set MUX3 address to 1/2 P1a
C = CONV * adc2.read()			# read MUX3 via ADC2
i2c.writeto_mem(addr, 1, b'\x04')  	# set MUX3 address to 1/2 P1b
D=  CONV * adc1.read()			# read MUX3 via ADC2
I1 = ( C - D ) / 3.3			# Voltage  over R1 = 3.3 OHM
print('P1 current=',round(I1,2),'Ampere')	# P1 should be aroind 0.1A            		
print('P1a=',round(C,2),'Volt')		# P1a should be around  3.6 Volt
print('P1b=',round(D,2),'Volt')		# P1b should be around 3.4 Volt
	# ------------------------------------------------
	# Measure P2 at VMM AUX connector
	# ------------------------------------------------
				# route pin y5  (P2 sense wire) of MUX1 to ADC1
i2c.writeto_mem(addr, 1, b'\x05') 	# set address to y5 pin
adc1 = ADC(Pin('X12')) 		# read ADC2 on pin X12 
	# --------------------------------------------
CONV1=0.00080566 		# conversion of ADC  bits in Volt	
P2=( CONV1 * adc1.read() ) - 0.05 # correct for 0.05 V GND dropoff over cable	 	
print('P2 at VMM=',round(P2,2),'Volt')		# P2 should be between 1.8 and 1.9Volt
	# ------------------------------------------------
	# Measure P1 at VMM AUX connector
	# ------------------------------------------------
				# route pin y6  (P1 sense wire) of MUX1 to ADC1
i2c.writeto_mem(addr, 1, b'\x06') 	# set address to y5 pin
adc1 = ADC(Pin('X12')) 		# read ADC2 on pin X12 
	# --------------------------------------------
CONV1=0.00080566 		# conversion of ADC  bits in Volt	
P1=( CONV1 * adc1.read() ) - 0.05 # correct for 0.05 V GND dropoff over cable	 	
print('P1 at VMM=',round(P1,2),'Volt')		# P1 should be between 2.9 and 3.3V