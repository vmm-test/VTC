""" 
    label_print.py

    Class for creating, viewing and printing labels for sticking on hybrid coolers.
    All sizes are set for this label sheet: https://www.avery.co.uk/product/multipurpose-labels-3666
    
    Method create takes position tuple that sets which label on the sheet is used.
    Dictionary data_dict takes report dictionary from the meas_result.py class with added hybrid name.

    Preview and print methods use OS specific utitilies, all except xdg-open should be preinstalled.

    Linux warning:
    Problem with loading the Lucida console font was reported on Linux. If it occurs, possibility is to use
    preinstalled fron in ReportLab like Courier, problem is worse readability on such small labels as the ones used.

"""

import sys
import labels
import os
import subprocess
import datetime
from reportlab.graphics import shapes
from reportlab.graphics.barcode import qr
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont
import reportlab


class Label():
    def __init__(self):
        self.label = None

    def create(self, position, data_dict, date=None):
        specs = labels.Specification(210, 297, 5, 13, 38, 21.16, top_margin=9.8, left_margin=10.2, column_gap=0, row_gap=0)
        # left margin could go to 10, top margin to be tried

        aux = {}
        aux["name"] = data_dict["name"]
        if data_dict["chip_id"] != None and len(data_dict["chip_id"]) > 16:
            aux["id"] = data_dict["chip_id"][-16:]
        else:
            aux["id"] = "no-ID"
        
        if date == None:
            aux["date"] = datetime.date.today().strftime("%d:%m:%y")
        else:
            aux["date"] = data_dict["create_date"].strftime("%d:%m:%y")
        aux["base1"] = round(float(data_dict["baseline_vmm0"]), 1)
        aux["base2"] = round(float(data_dict["baseline_vmm1"]), 1)
        
        aux["ok"] = True if data_dict["status"] == "OK" else False
        aux["bchan_num"] = data_dict["num_bad_ch_vmm0"] + data_dict["num_bad_ch_vmm1"]
        aux["bchan"] = data_dict["bad_channels"]
        aux["power"] = "" if data_dict["power_errors"] == None else data_dict["power_errors"]
        aux["i2c"] = "" if data_dict["i2c_errors"] == None else data_dict["i2c_errors"]
        aux["hdmi"] = "" if data_dict["hdmi_errors"] == None else data_dict["hdmi_errors"]

        registerFont(TTFont('Lucida Console', os.path.join(os.path.dirname(__file__), 'LUCON.ttf')))

        # callback for defining the layout of the label
        def draw_label(label, width, height, obj):

            fs = 5 # font size
            yrow = lambda x: 60 - x*fs - 2 # computes Y coordinate from input row number (starting from 1)

            label.add(shapes.String(2, yrow(1), "Name: {name}".format(**obj), fontName="Lucida Console", fontSize=fs))
            label.add(shapes.String(2, yrow(2), "Id:   {id}".format(**obj), fontName="Lucida Console", fontSize=fs))
            label.add(shapes.String(2, yrow(3), "Date: {date}".format(**obj), fontName="Lucida Console", fontSize=fs))
            label.add(shapes.String(2, yrow(4), "Baselines 0: < {base1} mV".format(**obj), fontName="Lucida Console", fontSize=fs))
            label.add(shapes.String(2, yrow(5), "Baselines 1: < {base2} mV".format(**obj), fontName="Lucida Console", fontSize=fs))
            
            qrcode = qr.QrCodeWidget("https://www.srstechnology.ch")
            qrcode.x = 69
            qrcode.y = 23
            qrcode.barHeight = 40
            qrcode.barWidth = 40
            label.add(qrcode)

            if obj["ok"]:
                label.add(shapes.String(2, yrow(7), "VTC check OK", fontName="Lucida Console", fontSize=fs))
            else:
                
                label.add(shapes.String(2, yrow(7), "Nr. bad channels: {bchan_num}".format(**obj), fontName="Lucida Console", fontSize=fs))
                label.add(shapes.String(2, yrow(8), "Bad chans: {bchan}".format(**obj), fontName="Lucida Console", fontSize=fs))
                label.add(shapes.String(2, yrow(9), "Power errs: {power}".format(**obj), fontName="Lucida Console", fontSize=fs))
                label.add(shapes.String(2, yrow(10), "I2C errs: {i2c}".format(**obj), fontName="Lucida Console", fontSize=fs))
                label.add(shapes.String(2, yrow(11), "HDMI errs: {hdmi}".format(**obj), fontName="Lucida Console", fontSize=fs))

        # creates the sheet and assigns the callback
        sheet = labels.Sheet(specs, draw_label, border=False)
        
        # fills the parttial_pos_list with all empty positions that precede the chosen position
        if position[0] < 1 or position[0] > 13 or position[1] < 1 or position[1] > 5:
            raise ValueError("Position argument must be 2-tuple in range from (1,1) to (13,5)")
        partial_pos_list = []

        for i in range(1, position[0]):     # first fill all completely empty rows
            for j in range(1, 6):
                partial_pos_list.append((i, j))
        for j in range(1, position[1]):     # then fill the final row up to chosen position (excluded)
            partial_pos_list.append((position[0], j))

        sheet.partial_page(1, partial_pos_list)

        # sheet.partial_page(1, ((1,3),(1,4),(1,5)))


        sheet.add_label(aux)
        self.label = sheet


    def print(self):
        if self.label == None:
            print("Create a label first")
            return
        
        self.label.save('label.pdf')
        if sys.platform == 'win32':
            subprocess.run(["PDFtoPrinter", "label.pdf"], shell=True).check_returncode()
        elif sys.platform == 'linux':
            subprocess.run(["lp", "label.pdf"], shell=True).check_returncode()
        else:
            subprocess.run(["lp", "label.pdf"], shell=False).check_returncode()

    def preview(self):
        if self.label == None:
            print("Create a label first")
            return
        
        self.label.save('label.pdf')
        if sys.platform == 'win32':
            # start
            subprocess.run(["start", "label.pdf"], shell=True).check_returncode()
        elif sys.platform == 'linux':
            # xdg-open
            subprocess.run(["xdg-open", "label.pdf"], shell=True).check_returncode()
        else:
            # open -n
            subprocess.run(["open", "-n", "label.pdf"], shell=False).check_returncode()
