""" 
    base_noise_ctrl.py

    NOT intended to integrate to GUI
    This is just to measure baseline noise easily through command line
    Uses existing scripts written for GUI
"""

import json
import gui_serial

import gui_cfg
import time

def setup(port='COM10'):
    cfg = gui_cfg.VMMconfig()
    cfg.mod('sm', 0)
    cfg.mod('sbfm', 1)
    cfg.mod('scmx', 1)
    cfg.mod('st', 3)
    cfg.mod('sbip', 1)
    cfg.mod('sdcks', 1)
    cfg.mod('sdt', 300)
    cfg.mod('sdp10', 500)
    gui_serial.serial_write('vmm_cfg.cfg = {}'.format(cfg.get()), port, timeout=0)
    time.sleep(0.001)
    gui_serial.import_module('vmm_base_noise', port)
    gui_serial.serial_write('vmm_base_noise.setup()', port, timeout=0)
    time.sleep(0.001)

def onoff(port='COM10'):
    gui_serial.import_module('vmm_power_cycle', port)
    gui_serial.serial_write('vmm_power_cycle.power_cycle()', port)

def measure(count, port='COM10'):
    # results1 = []
    results2 = []

    for _ in range(count):
        # ret = gui_serial.serial_write('vmm_base_noise.measure(72)', port)
        # results1.append(json.loads(ret[0]))
        ret = gui_serial.serial_write('vmm_base_noise.measure(73)', port)
        results2.append(json.loads(ret[0]))
        time.sleep(0.001)

    # return (results1, results2)
    return results2

def dumpcsv(datalist, filename):
    with open(filename, 'w') as f:
        length = len(datalist)
        for n, i in enumerate(datalist):
            if n != length-1:
                f.write('{},'.format(i))
            else:
                f.write(i)
