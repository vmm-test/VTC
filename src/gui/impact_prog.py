"""
    impact_prog.py

    Functions for programming FPGA and its flash through Impact

    If Impact is not in the PATH environment variable, its path must be supplied to the functions.

    Caller of these functions should catch exception if running Impact would fail.
"""

import subprocess

def prog_fpga(filename, impact_path=""):
    ext = filename.rpartition('.')[-1]
    if ext != "bit" and ext != "BIT":
        raise ValueError("Input bitstream file has to have .bit extension")

    template = """setMode -bscan
setCable -p auto
setCableSpeed -speed 30000000
addDevice -p 1 -file {}
program -p 1
quit
"""
    with open("vmm_impact_fpga.cmd", "w") as fout:
        fout.write(template.format(filename))

    # call impact with created file
    subprocess.run(["{}impact".format(impact_path), "-batch", "vmm_impact_fpga.cmd"]).check_returncode()


def prog_flash(filename, impact_path=""):
    ext = filename.rpartition('.')[-1]
    if ext != "mcs" and ext != "MCS":
        raise ValueError("Input bitstream file has to have .mcs extension")

    template = """setMode -bscan
setCable -p auto
setCableSpeed -speed 30000000
addDevice -p 1 -part xc6slx16
attachFlash -p 1 -spi AT45DB161E
assignfiletoattachedflash -p 1 -file {}
program -e -v -spionly -p 1
closeCable
quit
"""

    with open("vmm_impact_flash.cmd", "w") as fout:
        fout.write(template.format(filename))

    # call impact with created file
    subprocess.run(["{}impact".format(impact_path), "-batch", "vmm_impact_flash.cmd"]).check_returncode()