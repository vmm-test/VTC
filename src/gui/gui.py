"""
    gui.py

    Main file of the VTC control GUI application.
    
"""

import time
import json
from tkinter import *
from tkinter import messagebox as mb
from tkinter import font
from tkinter import filedialog
from tkinter import simpledialog
from tkinter.ttk import Button, Notebook, Radiobutton
import pdb
import sys
import serial
import datetime
import re

from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

import gui_cfg
import baseline
import impact_prog
import thresholds
import succ_indicator
import label_print
import meas_result
import gui_db_window
from gui_serial import serial_write, import_module
from gui_db import Report, Record, Baseline
from gui_db import search_db, delete_db, load_db


AUTHORS = ["Marek Hracek", "Sorina Popescu", "Vendula Maulerova"]

# from gui_db import Load
SIMULATION = False

# Last reading from VMM chip to be written into database
report_table = Report()
record_table = Record()
baseline_table = Baseline()

root = Tk()
root.geometry("1500x1100")
root.title("VMM GUI")

# Message field on the top
top_message_text = "    To start, connect HDMI cable to connector J2 on hybrid.  Serial port for connection: "

if SIMULATION:
    import simulation
    top_message_text = "SIMULATION"

topCanvas = Canvas(root, height=100, width=1000, highlightthickness=0)
topCanvas.pack(side=TOP,ipady=10)


message = Label(topCanvas, text=top_message_text, bd=1, relief=SUNKEN)
message.config(font=("Courier", 16))
message.pack(side=LEFT)

portDefault = StringVar()
eCOM = Entry(topCanvas, bg="#ff8a88", width="300",textvariable=portDefault)
if sys.platform=='linux':
    portDefault.set( "/dev/ttyACM0" ) #linux option
elif sys.platform == 'win32':
    portDefault.set( "COM10")
else:
    portDefault.set( "/dev/tty.usbmodem314F395D32382" )
eCOM.config(font=("Courier", 16))
eCOM.pack(side=RIGHT)

##########################################################
# Functions updating results widgets, all operate on container class with three dicts
# this way these functions can draw on just measured data or one pulled from DB

def update_power_i2c_gui(measurement):
    power_i2c = measurement.power_i2c

    aux = power_i2c.get("p1", "?")
    powerLabelP1["text"] = "P1 = {} A".format(aux)
    powerLabelP1["background"] = "#dfffd4" if isinstance(aux, float) and thresholds.power["p1"](aux) else "#ffc588"
    powerLabelP1a["text"] = "P1a = {} V".format(power_i2c.get("p1a", "?"))
    powerLabelP1b["text"] = "P1b = {} V".format(power_i2c.get("p1b", "?"))

    aux = power_i2c.get("p2", "?")
    powerLabelP2["text"] = "P2 = {} A".format(aux)
    powerLabelP2["background"] = "#dfffd4" if isinstance(aux, float) and thresholds.power["p2"](aux) else "#ffc588"
    powerLabelP2a["text"] = "P2a = {} V".format(power_i2c.get("p2a", "?"))
    powerLabelP2b["text"] = "P2b = {} V".format(power_i2c.get("p2b", "?"))

    aux = power_i2c.get("p1vmm", "?")
    powerLabelP1VMM["text"] = "P1 VMM = {} V".format(aux)
    powerLabelP1VMM["background"] = "#dfffd4" if isinstance(aux, float) and thresholds.power["p1vmm"](aux) else "#ffc588"
    aux = power_i2c.get("p2vmm", "?")
    powerLabelP2VMM["text"] = "P2 VMM = {} V".format(power_i2c.get("p2vmm", "?"))
    powerLabelP2VMM["background"] = "#dfffd4" if isinstance(aux, float) and thresholds.power["p2vmm"](aux) else "#ffc588"

    # I2C
    def set_i2c_label(label, base_text, val):
        label["text"] = base_text.format(val)
        label["background"] = "#dfffd4" if val != "?" and val != None else "#ffc588"

    set_i2c_label(i2cLabel2, "ID \n{}", power_i2c.get("chip_id", "?"))
    set_i2c_label(i2cLabel3, "EEPROM [{}]", power_i2c.get("eeprom", "?"))
    set_i2c_label(i2cLabel4, "ID chip [{}]", power_i2c.get("id_chip", "?"))
    set_i2c_label(i2cLabel5, "FPGA [{}]", power_i2c.get("fpga", "?"))
    set_i2c_label(i2cLabel6, "uADC1 [{}]", power_i2c.get("uadc1", "?"))
    set_i2c_label(i2cLabel7, "uADC2 [{}]", power_i2c.get("uadc2", "?"))

def update_baseline_gui(measurement, succ):
    if succ == True:
        output = measurement.baselines 

        rows_baseline = []
        rows_baseline.append(["channel", "VMM0", "VMM1", "channel", "VMM0", "VMM1"])
        for key in range (32):
            col2 = key + 32
            rows_baseline.append([str(key), str(output[key][0][0]), str(output[key][1][0]), str(col2), str(output[col2][0][0]), str(output[col2][1][0])])

        table_height = 33
        table_width = 6

        global baseline_frame
        baseline_frame.destroy()

        baseline_frame = Frame(textArea_baseline)
        baseline_frame.pack(fill=BOTH)
        
        for i in range(table_height):
            for j in range(table_width):
                b = Label(baseline_frame, text=rows_baseline[i][j])
                if i != 0 and j != 0 and j != 3:
                    if not thresholds.baseline(float(rows_baseline[i][j])):
                        b["background"] = "red"
                        b["foreground"] = "white"
                b.grid(row=i, column=j)

        for i in range(6):
            baseline_frame.columnconfigure(i, weight=1, minsize=70)
        
        global plot_frame
        plot_frame.destroy()

        plot_frame = Frame(plotArea_baseline)
        plot_frame.pack(fill=BOTH)

        # plot functions
        fig = Figure(figsize = (5, 5),  dpi = 100) 

        # data
        y0 = [output[i][0][0] for i in range(64)] 
        y1 = [output[i][1][0] for i in range(64)] 
        x = [i for i in range(64)]

        # adding the subplot 
        plot1 = fig.add_subplot(111) 

        # plotting the graph 

        plot1.scatter(x, y0, s=10, c='b', marker="s", label='VMM0')
        plot1.scatter(x,y1, s=10, c='g', marker="o", label='VMM1')
        plot1.legend()
        plot1.set_xlabel('VMM channel', fontsize=12)
        plot1.set_ylabel('baseline [mV]', fontsize=12)
        # plot1.set_ylim(0, 200)
        plot1.set_title('Baseline')

        fig.subplots_adjust(left=0.16, right=0.98)

        # creating the Tkinter canvas 
        # containing the Matplotlib figure 
        plot_canvas = FigureCanvasTkAgg(fig,   master = plot_frame)   
        plot_canvas.draw() 

        # placing the canvas on the Tkinter window 
        plot_canvas.get_tk_widget().pack(ipadx=20) 

        # creating the Matplotlib toolbar 
        toolbar = NavigationToolbar2Tk(plot_canvas,  plot_frame) 
        toolbar.update() 
    else:
        # global baseline_frame
        baseline_frame.destroy()
        baseline_frame = Frame(textArea_baseline)
        baseline_frame.pack(fill=BOTH)
        Label(baseline_frame, text="Measuring baselines failed!").pack()

        # global plot_frame
        plot_frame.destroy()
        plot_frame = Frame(plotArea_baseline)
        plot_frame.pack(fill=BOTH)
        Label(plot_frame, text="Measuring baselines failed!").pack()

def update_mux_gui(measurement):
    mux = measurement.mux
    rows = []

    def natural_sort(kv):
        """ Sort dictionary items naturally"""
        convert = lambda text: int(text) if text.isdigit() else text.lower()
        return [convert(c) for c in re.split('([0-9]+)', kv[0])]
    index=0
    print(mux)
    print("\n")
    print(sorted(mux.items(), key=natural_sort))
    for key, values in sorted(mux.items(), key=natural_sort):
        # skipping the ground index and I2C pins
        if (index==7):
            index+=1
        if index == 14:
            index = 16

        # key is "pin1", "pin2", "aux", "pin19". ...
        # values is a dict containing U, R and Runits

        # Create list of lines for Measurement Field
        rows.append("{:9s}\t{:5.2f} V\t{:5.2f} {:4s}\t{}".format(key, values["U"], values["R"], values["Runits"], thresholds.j2_str[key]))

        # Find Pin to update
        if key in j2pins:
            # Find function which tells if Pin is OK or Error
            func = thresholds.j2.get(key, lambda x: False)

            # Check if Pin is OK or Error
            ok = func(values["R"])
            if ok:
                if key=="pin14":
                    j2pins[key]["text"] = "Pin14/19 "+pinDescription[index]+" OK"
                    j2pins[key]["bg"] = "#dfffd4"
                else:
                    j2pins[key]["text"] = "{} ".format(key.capitalize())+pinDescription[index]+" OK"
                    j2pins[key]["bg"] = "#dfffd4"
            else:
                j2pins[key]["text"] = "{} ".format(key.capitalize())+pinDescription[index]+" Error"
                j2pins[key]["bg"] = "#ffc588"
            index+=1

    if measurement.power_i2c.get("uadc1","?") in (72, "ok"):
        j2pins["pin15"]["text"] = "Pin15 ".format(key=key.capitalize())+pinDescription[14]+" OK"
        j2pins["pin15"]["bg"] = "#dfffd4"
    else:
        j2pins["pin15"]["text"] = "Pin15 ".format(key=key.capitalize())+pinDescription[14]+" Error"
        j2pins["pin15"]["bg"] = "#ffc588"

    if measurement.power_i2c.get("uadc2","?") in (73, "ok"):
        j2pins["pin16"]["text"] = "Pin16 ".format(key=key.capitalize())+pinDescription[15]+" OK"
        j2pins["pin16"]["bg"] = "#dfffd4"
    else:
        j2pins["pin16"]["text"] = "Pin16 ".format(key=key.capitalize())+pinDescription[15]+" Error"
        j2pins["pin16"]["bg"] = "#ffc588"

    textArea["text"] = "\n".join(rows)

############################################################################
#
# Column 1 | 3 big red buttons
#
############################################################################

def col1Button1(event):
    print("Clicked Start J2 Connection")

    # Change button collor
    oval = connCanvas.find_withtag("Button1")
    connCanvas.itemconfig(oval, fill="#ffc588")
    connCanvas.update()

    if not SIMULATION:
        try:
            output = serial_write("vmm_request()", port=eCOM.get(), timeout=1)[0]
            if output != "vmm_reply":
                raise ValueError
        except:
            mb.showerror("Serial port error", "VTC doesn't respond, check whether you entered correct serial port and VTC is connected to PC.")
            connCanvas.itemconfig(oval, fill="#ff8a88")
            connCanvas.update()
            return

    # New record to be written into database
    global report_table, record_table, baseline_table
    report_table = Report()
    record_table = Record()
    baseline_table = Baseline()

    # global meas_data
    meas_data = meas_result.Measurement()

    # Call microcontroller

    # Power and I2C (P1, P2, chip id, chip addr, eeprom, ...)
    if SIMULATION:
        output = simulation.p1p2id
    else:
        import_module("vmm_p1p2id", port=eCOM.get())
        PowerState.set(True)
        auxCanvas.update()

        output = serial_write("vmm_p1p2id.boot_test()", port=eCOM.get(), timeout=7)
        answer = ""
        try:
            boot_res = json.loads(output[0])
        except Exception as e:
            print("FPGA doesn't respond, terminating whole test")
            print(output)
            print(e)
            boot_res= {"vmm0" : "", "vmm1" : ""}
            answer = mb.askquestion("Boot validation error", "Boot validation failed, please try the test again, if this error keeps appearing, " +
                                                  "check the voltages P1 and P2 and correct them with VTC trimmers if necessary. " +
                                                  "If this doesn't help, there's a high chance of connection/soldering issue with connector and/or FPGA.\n\nDo you still want to continue with the test?", icon=mb.WARNING, type=mb.YESNO)
            if answer == 'no':
                import_module("vmm_power_cycle", port=eCOM.get())
                serial_write("vmm_power_cycle.power_cycle()", port=eCOM.get(), timeout=1)
                PowerState.set(not PowerState.get())
                connCanvas.itemconfig(oval, fill="#ff8a88")
                connCanvas.update()
                return

        if answer != "no" and (boot_res["vmm0"] != "ok" or boot_res["vmm1"] != "ok"):
            def gen_msg(x):
                if x == "ok":
                    return "booted and configured correctly"
                elif x == "boot":
                    return "failed booting"
                elif x == "cfg":
                    return "booted correctly, but failed configuration"
                else:
                    return ""
            answer = mb.askquestion("Boot validation error", "Boot validation failed:\n\nVMM0: {}\nVMM1: {}\n\nIf only configuration failed, try again, otherwise possible solution could be increasing power supply voltage.\n\nDo you still want to continue with the test?".format(gen_msg(boot_res["vmm0"]), gen_msg(boot_res["vmm1"])), icon=mb.WARNING, type=mb.YESNO)
            if answer == "no":
                connCanvas.itemconfig(oval, fill="#ff8a88")
                connCanvas.update()
                PowerState.set(False)
                return

        output = serial_write("vmm_p1p2id.readout()", port=eCOM.get(), timeout=1)
       

    print(output)

    power_i2c = {}

    try:
        power_i2c = json.loads(output[0])

        chip_id = power_i2c.get("chip_id", "?")
        
        #Write chip_id to report table
        report_table.fill_chip_id(chip_id)
        
        #Get ID from report table
        id = report_table.get_id()

        # Fill Record object with id and data of current reading from VMM
        record_table.fill_data(id, power_i2c)

        # save collected data and draw it
        meas_data.power_i2c = power_i2c
        update_power_i2c_gui(meas_data)

    except Exception as e:
        print("Problem parsing p1p2 output")

    
    #################### Baseline measurements##########################
    try:
        if SIMULATION:
            output = simulation.baseline
        else:
            measurements=baseline.Baseline(port=eCOM.get())
            measurements.measure_allchan(2,3, auto_resolution=True)
            output=measurements.get_avg('all')    # Update Baseline measurements field
        
        baseline_table.fill_data(id, output)

        meas_data.baselines = output
        update_baseline_gui(meas_data, True)
    except:
        print("Problem with measuring of baselines")
        update_baseline_gui(meas_data, False)


    ####### Temperature measurement ########
    if not SIMULATION:
        temp_measurement()

    ####### MUX 1 ###########
    mux = {}

    try:
        if SIMULATION:
            output = simulation.mux1
        else:
            import_module("vmm_mux1", port=eCOM.get())
            PowerState.set(False)
            auxCanvas.update()
            output = serial_write("vmm_mux1.readout()", port=eCOM.get(), timeout=10)

        print(output)
        mux1 = json.loads(output[0])
        print('json finished')
#         Fill Record object with data of current reading from VMM
        record_table.fill_data(id, mux1)

        mux.update(mux1)
    except:
        print("Problem parsing mux1 output")
    ######### MUX3 ############
    try:
        if SIMULATION:
            output = simulation.mux3
        else:
            import_module("vmm_mux3", port=eCOM.get())
            PowerState.set(False)
            auxCanvas.update()
            output = serial_write("vmm_mux3.readout()", port=eCOM.get(), timeout=5)

        print(output)
        mux3 = json.loads(output[0])

        # Fill Record object with data of current reading from VMM
        record_table.fill_data(id, mux3)

        mux.update(mux3)
    except:
        print("Problem parsing mux3 output")
  
    meas_data.mux = mux
    update_mux_gui(meas_data)

    # create report values and store them in the table
    meas_data.create_report()
    report_table.fill_data(meas_data.report)
    
    # store the measured values and report in the permanent instance
    meas_result.data = meas_data

    # Change button collor back
    connCanvas.itemconfig(oval, fill="#ff8a88")
    connCanvas.update()


def col1Button2(event):
    print("Clicked Print Label")

    # Change button collor
    oval = connCanvas.find_withtag("Button2")
    connCanvas.itemconfig(oval, fill="#ffc588")
    connCanvas.update()

    if meas_result.data.report == {}:
        mb.showerror("No label to print", "You must make a measurement (button Execute full test) or load one from the database to print a label.")
    else:
        # if meas_data.label.label == None:
        meas_result.data.label.create([int(labelRow.get()),int(labelColumn.get())], {**meas_result.data.report, "name":hybrid_name.get()}, date=True)
        meas_result.data.label.print()

    # Change button collor back
    connCanvas.itemconfig(oval, fill="#ff8a88")
    connCanvas.update()

fileFPGAPath = StringVar()
fileFlashPath = StringVar()
ImpactPath = StringVar()


def col1Button3(event):
    print("Clicked Program FPGA")

    # Change button collor
    oval = connCanvas.find_withtag("Button3")
    connCanvas.itemconfig(oval, fill="#ffc588")

    if PowerState.get():
        succFPGA.loading()
        connCanvas.update()

        try:
            impact_prog.prog_fpga(fileFPGAPath.get(), ImpactPath.get())
            succFPGA.succ()
        except:
            print("FPGA program fail")
            succFPGA.fail()
    else:
        mb.showwarning(icon='warning', title='Problem with programming FPGA',
            message='For programming FPGA the hybrid must be turned on, please turn on the VTC with the button VMM Power On/Off.')


    # Change button collor back
    connCanvas.itemconfig(oval, fill="#ff8a88")
    connCanvas.update()

def col1Button4(event):
    print("Clicked Program Flash")

    # Change button collor
    oval = connCanvas.find_withtag("Button4")
    connCanvas.itemconfig(oval, fill="#ffc588")

    if PowerState.get():
        succFlash.loading()
        connCanvas.update()

        try:
            impact_prog.prog_flash(fileFlashPath.get(), ImpactPath.get())
            succFlash.succ()
        except:
            print("Flash program fail")
            succFlash.fail()
    else:
        mb.showwarning(icon='warning', title='Problem with programming Flash',
            message='For programming Flash the hybrid must be turned on, please turn on the VTC with the button VMM Power On/Off.')


    # Change button collor back
    connCanvas.itemconfig(oval, fill="#ff8a88")
    connCanvas.update()

colFrame1 = Frame(root, height=1000, width=200)
colFrame1.pack(side=LEFT, anchor=N, padx=10)

connCanvas = Canvas(colFrame1, height=1000, width=200, highlightthickness=0)
connCanvas.pack()

connY1 = 30

col1Button1Oval = connCanvas.create_oval(50, connY1, 150, connY1+100, fill="#ff8a88", outline="#ff3f3c", width=2, tag="Button1")
col1Button1Text = connCanvas.create_text(100, connY1+50, text="     Execute\ncomplete test")
connCanvas.tag_bind(col1Button1Oval, "<Button-1>", col1Button1)
connCanvas.tag_bind(col1Button1Text, "<Button-1>", col1Button1)

connectorSelect = BooleanVar()
col1RadioButtonJ2 = connCanvas.create_window(90, connY1+120, window=Radiobutton(connCanvas, text="J2 connector", state=DISABLED, value=False, variable=connectorSelect))
col1RadioButtonJ3 = connCanvas.create_window(90, connY1+140, window=Radiobutton(connCanvas, text="J3 connector", state=DISABLED, value=True, variable=connectorSelect))

hybrid_name = StringVar()
connCanvas.create_text(100, connY1+170, text="Hybrid name:")
connCanvas.create_window(100, connY1+190, window=Entry(connCanvas, textvariable=hybrid_name))
hybrid_name.set("Name")

connY2 = 280
col1Button2Oval = connCanvas.create_oval(50, connY2, 150, connY2+100, fill="#ff8a88", outline="#ff3f3c", width=2, tag="Button2")
col1Button2Text = connCanvas.create_text(100, connY2+50, text="Print Label")
connCanvas.tag_bind(col1Button2Oval, "<Button-1>", col1Button2)
connCanvas.tag_bind(col1Button2Text, "<Button-1>", col1Button2)

labelRow = StringVar()
labelColumn = StringVar()
def label_pos_validate(text, lim):
    if len(text) == 0:
        return True
    try:
        aux = int(text)
        if aux < 1 or aux > lim:
            return False
        return True
    except:
        return False
row_validate = connCanvas.register(lambda *args: label_pos_validate(*args, 13))
column_validate = connCanvas.register(lambda *args: label_pos_validate(*args, 5))

connCanvas.create_text(80, connY2+120, text="Row (1-13):")
connCanvas.create_window(140, connY2+120, window=Entry(connCanvas, width=3, textvariable=labelRow, validate="key", vcmd=(row_validate, "%P"), invalidcommand=connCanvas.bell))
connCanvas.create_text(80, connY2+140, text="Column (1-5):")
connCanvas.create_window(140, connY2+140, window=Entry(connCanvas, width=3, textvariable=labelColumn, validate="key", vcmd=(column_validate, "%P"), invalidcommand=connCanvas.bell))

labelRow.set(1)
labelColumn.set(1)


def choose_file(textvar, filetype):
    if filetype == "bit":
        aux = filedialog.askopenfilename(filetypes=[("Bit files", ".bit"), ("All files", "*")])
    elif filetype == "mcs":
        aux = filedialog.askopenfilename(filetypes=[("MCS files", ".mcs"), ("All files", "*")])
    elif filetype == "dir":
        aux = filedialog.askdirectory(mustexist=True)
        if aux[-1] != '/':
            aux = aux + '/'
    else:
        raise ValueError("Second arg to choose_file must be 'bit', 'mcs' or 'dir'")
    
    if aux != "":
        textvar.set(aux)

connY3 = 480
col1Button3Oval = connCanvas.create_oval(50, connY3, 150, connY3+100, fill="#ff8a88", outline="#ff3f3c", width=2, tag="Button3")
col1Button3Text = connCanvas.create_text(100, connY3+50, text="Program FPGA")
connCanvas.tag_bind(col1Button3Oval, "<Button-1>", col1Button3)
connCanvas.tag_bind(col1Button3Text, "<Button-1>", col1Button3)

succFPGA = succ_indicator.SuccIndicator(connCanvas, 133, connY3+40, 15)

col1Entry1 = connCanvas.create_window(70, connY3+120, window=Entry(connCanvas, width=14, textvariable=fileFPGAPath))
col1FileDialog1 = connCanvas.create_window(140, connY3+120, window=Button(connCanvas, text="Browse", width=7, command=lambda:choose_file(fileFPGAPath, 'bit')))


connY4 = 650
col1Button4Oval = connCanvas.create_oval(50, connY4, 150, connY4+100, fill="#ff8a88", outline="#ff3f3c", width=2, tag="Button4")
col1Button4Text = connCanvas.create_text(100, connY4+50, text="Program Flash")
connCanvas.tag_bind(col1Button4Oval, "<Button-1>", col1Button4)
connCanvas.tag_bind(col1Button4Text, "<Button-1>", col1Button4)

col1Entry2 = connCanvas.create_window(70, connY4+120, window=Entry(connCanvas, width=14, textvariable=fileFlashPath))
col1FileDialog2 = connCanvas.create_window(140, connY4+120, window=Button(connCanvas, text="Browse", width=7, command=lambda:choose_file(fileFlashPath, 'mcs')))

succFlash = succ_indicator.SuccIndicator(connCanvas, 133, connY4+40, 15)

# text for Impact path dialog
connCanvas.create_text(100, connY4+190, text="Path to Impact folder\n(not needed if in PATH):")
col1Entry3 = connCanvas.create_window(70, connY4+220, window=Entry(connCanvas, width=14, textvariable=ImpactPath))
col1FileDialog3 = connCanvas.create_window(140, connY4+220, window=Button(connCanvas, text="Browse", width=7, command=lambda:choose_file(ImpactPath, 'dir')))


# authors list
authors_font = font.Font(family="TkDefaultFont", size=10, weight=font.BOLD)
authors_text = "\n".join(AUTHORS)
connCanvas.create_text(95, connY4+320, text=authors_text, font=authors_font)

connCanvas.update()

############################################################################
#
# Column 2 | J2 default port
#
############################################################################

colFrame2 = Frame(root, height=800, width=150)
colFrame2.pack_propagate(0)
colFrame2.pack(side=LEFT, anchor=N, padx=10)

frameLabelcol2 = Label(colFrame2, text="J2 (default port)")
frameLabelcol2.pack(side=TOP, padx=5, pady=5)

# Inner frame of column2 with color
colFrame2inner = Frame(colFrame2, bg="#ffffd4", bd=1, relief=SOLID)
colFrame2inner.pack(side=TOP, fill=BOTH)

j2pins = {} # dictionary holding pin labels

pinDescription = [
"data1_p",
"GND-A",
"data1_n",
"data2_p",
"GND-B",
"data2_n",
"trg/ctrl_p",
"ground",
"trg/ctrl_n",
"clk_p",
"GND-C",
"clk_n",
"M/S",
"1/2 P2b",
"SCL",
"SDA",
"GND-D",
"1/2 P1b",
"1/2 P2b"
]
for n,i in enumerate(pinDescription):
    pinDescription[n] = "(" + i + ")"

for i in range(1, 19):
    if i==8:
        l = Label(colFrame2inner, text="Pin%d %s"%(i, pinDescription[i-1]), bg="#dfffd4", bd=1, relief=SOLID)
    elif i==14:
        l = Label(colFrame2inner, text="Pin14/19 %s [?]"%(pinDescription[i-1]), bg="#dfffd4", bd=1, relief=SOLID)
    else:
        l = Label(colFrame2inner, text="Pin%d %s [?]"%(i, pinDescription[i-1]), bg="#dfffd4", bd=1, relief=SOLID)
    l.pack(ipadx=25, ipady=5, padx=5, pady=5, fill=X)
    j2pins["pin{}".format(i)] = l

############################################################################
#
# Column 3
#
############################################################################

colFrame3 = Frame(root, height=750, width=250)
colFrame3.pack_propagate(0)
colFrame3.pack(side=LEFT, anchor=N, padx=10)

# Inner Frame TOP (power P1,P2)

frameLabelcol3top = Label(colFrame3, text="Power P1, P2")
frameLabelcol3top.pack(side=TOP, padx=5, pady=5)

colFrame3inner = Frame(colFrame3, bg="#ffffd4", bd=1, relief=SOLID)
colFrame3inner.pack(side=TOP, fill=BOTH)

powerLabelP1 = Label(colFrame3inner, text="P1 = [?] A", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP1.pack(ipadx=25, ipady=5, padx=5, pady=5, fill=X)

powerLabelP1a = Label(colFrame3inner, text="P1a = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP1a.pack(ipadx=25, ipady=5, padx=5, pady=5, fill=X)

powerLabelP1b = Label(colFrame3inner, text="P1b = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP1b.pack(ipadx=20, ipady=5, padx=5, pady=5, fill=X)

powerLabelP2 = Label(colFrame3inner, text="P2 = [?] A", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP2.pack(ipadx=25, ipady=5, padx=5, pady=5, fill=X)

powerLabelP2a = Label(colFrame3inner, text="P2a = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP2a.pack(ipadx=25, ipady=5, padx=5, pady=5, fill=X)

powerLabelP2b = Label(colFrame3inner, text="P2b = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP2b.pack(ipadx=20, ipady=5, padx=5, pady=5, fill=X)

powerLabelP1VMM = Label(colFrame3inner, text="P1 VMM = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP1VMM.pack(ipadx=20, ipady=5, padx=5, pady=5, fill=X)

powerLabelP2VMM = Label(colFrame3inner, text="P2 VMM = [?] V", bg="#dfffd4", bd=1, relief=SOLID)
powerLabelP2VMM.pack(ipadx=20, ipady=5, padx=5, pady=5, fill=X)

# Inner Frame I2C scan VMM

frameLabelcol3bottom = Label(colFrame3, text="I2C scan VMM")
frameLabelcol3bottom.pack(side=TOP, padx=5, pady=5)

colFrame3inner2 = Frame(colFrame3, bg="#ffffd4", bd=1, relief=SOLID)
colFrame3inner2.pack(side=TOP, fill=BOTH)

# i2cLabel1 = Label(colFrame3inner2, text="IC chip [?]", bg="#dfffd4", bd=1, relief=SOLID)
# i2cLabel1.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel2 = Label(colFrame3inner2, text="ID [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel2.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel3 = Label(colFrame3inner2, text="EEprom [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel3.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel4 = Label(colFrame3inner2, text="ID chip [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel4.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel5 = Label(colFrame3inner2, text="FPGA [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel5.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel6 = Label(colFrame3inner2, text="uADC 1 [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel6.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)

i2cLabel7 = Label(colFrame3inner2, text="uADC 2 [?]", bg="#dfffd4", bd=1, relief=SOLID)
i2cLabel7.pack(ipadx=10, ipady=5, padx=5, pady=5, fill=X)


############################################################################
#
# Column 4
#
############################################################################

colFrame4 = Frame(root, height=750, width=200)
colFrame4.pack(side=LEFT, anchor=N, padx=10)

frameLabelcol4 = Label(colFrame4, text="J3 (Slave port)")
frameLabelcol4.pack(side=TOP, padx=5, pady=5)

# Inner frame of column2 with color
colFrame4inner = Frame(colFrame4, bg="#c4c4c4", bd=1, relief=SOLID)
colFrame4inner.pack(side=TOP, fill=BOTH)

for i in range(0, 19):

    if i == 2:
        l = Label(colFrame4inner, text="Pin%d Error"%(i+1), bg="#919191", bd=1, relief=SOLID)
        l.pack(ipadx=15, ipady=5, padx=5, pady=5)
    else:
        l = Label(colFrame4inner, text="Pin%d OK"%(i+1), bg="#ababab", bd=1, relief=SOLID)
        l.pack(ipadx=25, ipady=5, padx=5, pady=5)

############################################################################
#
# Column 5 Measurement field
#
############################################################################

colFrame5 = Frame(root, height=1050, width=500)
colFrame5.pack_propagate(0)
colFrame5.pack(side=LEFT, anchor=N, padx=10)

frameLabelcol5 = Label(colFrame5, text="Measurement field")
frameLabelcol5.pack(side=TOP, padx=5, pady=5)

longtext = "No Data"
textArea = Message(colFrame5, text=longtext, bd=1, relief=SOLID)
textArea.pack(fill=BOTH)#

# Column 5 Measurement field baseline
#
frameLabelcol5_baseline = Label(colFrame5, text="Baseline measurement field [mV]")
frameLabelcol5_baseline.pack(side=TOP, padx=5, pady=5)

baseline_tabparent = Notebook(colFrame5)
baseline_tabparent.pack(fill=BOTH)

textArea_baseline = Frame(baseline_tabparent, bd=1, relief=SOLID)
plotArea_baseline = Frame(baseline_tabparent, bd=1, relief=SOLID)

baseline_tabparent.add(plotArea_baseline, text="Plot")
baseline_tabparent.add(textArea_baseline, text="Table")


# helper frames inside another frame so this can be erased together with the labels it contains
plot_frame = Frame(plotArea_baseline)
plot_frame.pack(fill=BOTH)
Label(plot_frame, text="No Data").pack()

baseline_frame = Frame(textArea_baseline)
baseline_frame.pack(fill=BOTH)
Label(baseline_frame, text="No Data").pack()


############################################################################
#
# Column 6 Aux buttons
#
############################################################################

colFrame6 = Frame(root, height=750, width=200)
colFrame6.pack(side=LEFT, anchor=N, padx=10)

frameLabelcol6 = Label(colFrame6, text="Aux. buttons")
frameLabelcol6.pack(side=TOP, padx=5, pady=5)

auxCanvas = Canvas(colFrame6, height=850, width=200)
auxCanvas.pack()

def col6Button1(event):
    print("Clicked VMM Power On/Off")

    # Change button collor
    oval = auxCanvas.find_withtag("Button1")
    auxCanvas.itemconfig(oval, fill="#ffc588")
    auxCanvas.update()

    # check if pyboard is responding
    try:
        output = serial_write("vmm_request()", port=eCOM.get(), timeout=1)[0]
        if output != "vmm_reply":
            raise ValueError
    except:
        mb.showerror("Serial port error", "VTC doesn't respond, check whether you entered correct serial port and VTC is connected to PC.")
        auxCanvas.itemconfig(oval, fill="#88c2ff")
        auxCanvas.update()
        return

    # Call microcontroller
    import_module("vmm_power_cycle", port=eCOM.get())
    output = serial_write("vmm_power_cycle.power_cycle()", port=eCOM.get(), timeout=5)
    print(output)
    PowerState.set(not PowerState.get())

    # Change button collor back
    auxCanvas.itemconfig(oval, fill="#88c2ff")
    auxCanvas.update()

def col6Button2(event):
    print("Clicked VMM ID Readout")

    # Change button collor
    oval = auxCanvas.find_withtag("Button2")
    auxCanvas.itemconfig(oval, fill="#ffc588")
    auxCanvas.update()

    # Call microcontroller
    import_module("vmm_chip_readout_sp1", port=eCOM.get())
    output = serial_write("vmm_chip_readout_sp1.id_readout()", port=eCOM.get(), timeout=1)
    print(output)

    # Change button collor back
    auxCanvas.itemconfig(oval, fill="#88c2ff")
    auxCanvas.update()

def col6Button3(event):
    print("Clicked Label preview")

    # Change button collor
    oval = auxCanvas.find_withtag("Button3")
    auxCanvas.itemconfig(oval, fill="#ffc588")
    auxCanvas.update()

    if meas_result.data.report == {}:
        mb.showerror("No label to preview", "You must make a measurement (button Execute full test) or load one from the database to preview result label.")
    else:
        # if meas_data.label.label == None:
        meas_result.data.label.create([int(labelRow.get()),int(labelColumn.get())], {**meas_result.data.report, "name":hybrid_name.get()}, date=True)
        meas_result.data.label.preview()

    # Change button collor back
    auxCanvas.itemconfig(oval, fill="#88c2ff")
    auxCanvas.update()

def col6Button4(event):    
    print("Clicked DB management")
    oval = auxCanvas.find_withtag("Button4")
    auxCanvas.itemconfig(oval, fill="#ffc588")
    auxCanvas.update()

    def gui_update_callback(*unused):
        update_power_i2c_gui(meas_result.data)
        update_baseline_gui(meas_result.data, len(meas_result.data.baselines) != 0)
        update_mux_gui(meas_result.data)
        hybrid_name.set(meas_result.data.report.get("meas_descr", ""))

    gui_update = BooleanVar()
    gui_update.trace_add('write', gui_update_callback)

    gui_db_window.DbMng(hybrid_name=hybrid_name, report_table=report_table, record_table=record_table, baseline_table=baseline_table, gui_update=gui_update)


    # Change button collor back 
    auxCanvas.itemconfig(oval, fill="#88c2ff")
    auxCanvas.update()
    

def col6Button5(event):
    print("Read temperature")

    # Change button collor
    oval = auxCanvas.find_withtag("Button5")
    auxCanvas.itemconfig(oval, fill="#ffc588")
    auxCanvas.update()

    if PowerState.get():
        temp_measurement()
    else:
        mb.showwarning(icon='warning', title='Problem with measuring temperature',
            message='For measuring temperature the hybrid must be turned on, please turn on the VTC with the button VMM Power On/Off and wait 3 seconds.')

    # Change button collor back
    auxCanvas.itemconfig(oval, fill="#88c2ff")
    auxCanvas.update()

def temp_measurement():
    cfg = gui_cfg.VMMconfig()
    cfg.mod('sm', 4)
    cfg.mod('sbfm', 1)
    cfg.send(eCOM.get())

    vals = serial_write('vmm_temp.readout()', port=eCOM.get(),timeout=3)
    print(vals)
    try:
        while True:
            try:
                vals = json.loads(vals[0])
                break
            except json.decoder.JSONDecodeError:
                vals = vals[1:]
               
        temps = [round(vals[1], 2), round(vals[3], 2)]
        # temperature_text['text'] = str(vals)
        temp_descr = "VMM0: {} °C\nVMM1: {} °C".format(temps[0], temps[1])
        auxCanvas.itemconfigure(temperature_text, text=temp_descr )
    except:
        auxCanvas.itemconfigure(temperature_text, text="Can't find uADC\nor VMM reconfig err" )


auxY1 = 30
col6Button1Oval = auxCanvas.create_oval(30, auxY1, 130, auxY1+100, fill="#88c2ff", outline="#00bfff", width=2, tag="Button1")
col6Button1Text = auxCanvas.create_text(80, auxY1+50, text="VMM Power\nOn / Off")
auxCanvas.tag_bind(col6Button1Oval, "<Button-1>", col6Button1)
auxCanvas.tag_bind(col6Button1Text, "<Button-1>", col6Button1)

col6PowerOval = auxCanvas.create_oval(140, auxY1+40, 150, auxY1+50, outline="black", tag="Power")
PowerState = BooleanVar()
PowerState.trace_add('write', lambda name, *args: auxCanvas.itemconfigure(col6PowerOval, fill="green2") if root.getvar(name) else auxCanvas.itemconfigure(col6PowerOval, fill="red"))

try:
    import_module("vmm_power_cycle", port=eCOM.get())
    power_read = serial_write("vmm_power_cycle.status()", port=eCOM.get(), timeout=1)

    if json.loads(power_read[0])[2] == 1:
        PowerState.set(True)
    else:
        PowerState.set(False)
except serial.serialutil.SerialException:
    pass

auxY2 = 180
col6Button2Oval = auxCanvas.create_oval(30, auxY2, 130, auxY2+100, fill="#c4c4c4", outline="#919191", width=2, tag="Button2")
col6Button2Text = auxCanvas.create_text(80, auxY2+50, text="   VMM\nID Readout")
# auxCanvas.tag_bind(col6Button2Oval, "<Button-1>", col6Button2)
# auxCanvas.tag_bind(col6Button2Text, "<Button-1>", col6Button2)

auxY3 = 330
col6Button3Oval = auxCanvas.create_oval(30, auxY3, 130, auxY3+100, fill="#88c2ff", outline="#00bfff", width=2, tag="Button3")
col6Button3Text = auxCanvas.create_text(80, auxY3+50, text="Label preview")
auxCanvas.tag_bind(col6Button3Oval, "<Button-1>", col6Button3)
auxCanvas.tag_bind(col6Button3Text, "<Button-1>", col6Button3)

auxY4 = 480
col6Button4Oval = auxCanvas.create_oval(30, auxY4, 130, auxY4+100, fill="#88c2ff", outline="#00bfff", width=2, tag="Button4")
col6Button4Text = auxCanvas.create_text(80, auxY4+50, text="Database\nmanagement")
auxCanvas.tag_bind(col6Button4Oval, "<Button-1>", col6Button4)
auxCanvas.tag_bind(col6Button4Text, "<Button-1>", col6Button4)

auxY5 = 630
col6Button5Oval = auxCanvas.create_oval(30, auxY5, 130, auxY5+100, fill="#88c2ff", outline="#00bfff", width=2, tag="Button5")
col6Button5Text = auxCanvas.create_text(80, auxY5+50, text="Measure\ntemperature")
auxCanvas.tag_bind(col6Button5Oval, "<Button-1>", col6Button5)
auxCanvas.tag_bind(col6Button5Text, "<Button-1>", col6Button5)

temperature_text = auxCanvas.create_text(80, auxY5+130, text=" No measured\ntemperature yet")
auxCanvas.create_rectangle(30, auxY5+110, 130, auxY5+150)

auxCanvas.update()

root.mainloop()
