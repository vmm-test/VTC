""" 
    succ_indicator.py
    
    Tkinter element drawn on Canvas.
    Indicates either:
        - success (green checkmark)
        - failure (red cross)
        - loading/running process (blue circle)

"""

class SuccIndicator():
    def __init__(self, canvas, x, y, size=30):
        self.canvas = canvas
        self.x = x
        self.y = y
        self.size = size
        self.type = None
        self.lines = []

    def succ(self):
        if self.type != None:
            self.nothing()
        x,y,size = self.x, self.y, self.size
        coords = [x, y+(size*2/3), x+(size/3), y+size, x+size, y]
        self.lines.append(self.canvas.create_line(coords, fill="green2", width=4))
        self.type = "succ"

    def fail(self):
        if self.type != None:
            self.nothing()

        x,y,size = self.x, self.y, self.size
        self.lines.append(self.canvas.create_line([x, y, x+size, y+size], fill="red", width=4))
        self.lines.append(self.canvas.create_line([x, y+size, x+size, y], fill="red", width=4))
        self.type = "fail"

    def loading(self):
        if self.type != None:
            self.nothing()

        x,y,size = self.x, self.y, self.size
        self.lines.append(self.canvas.create_oval([x, y, x+size, y+size], outline="blue", width=4))
        self.type = "loading"

    def nothing(self):
        for i in self.lines:
            self.canvas.delete(i)
        self.lines = []
        self.type = None

