import time
import serial

#port = "/dev/tty.usbmodem314F395D32382"

baudrate = 115200

def import_module(module_name, port):
    serial_write("import sys", port, 0)
    serial_write("del sys.modules['{}']".format(module_name),port, 0)
    time.sleep(0.1)
    serial_write("import {}".format(module_name), port, 0)
    time.sleep(0.1)

def serial_write(command, port, timeout=1):
    """
        Parameters
        ----------
        command : str
            python command to execute on a microcontroller
        
        timeout : int (float supported)
            read timeout in seconds
    """

    if timeout == None:
        # Do not allow "wait forever"
        timeout = 0

    with serial.Serial(port, baudrate, timeout=timeout) as ser:
      #  print(command)
        # Sending a command
        ser.write('{}\r'.format(command).encode())
        
        output = ser.read_until("0xDEADBEEF".encode())
        lines = output.decode().split("\r\n")

        # drop first and last lines. first is command itself, last is deadbeef
        # print("Ret: {}".format(lines))
        ret = lines[1:-1]
        ret = [x for x in ret if (x != '' and x !='MU1_3')] #fix for centos

        return ret


# possible alternative (not finished) for communication between PC and pyboard
# (could be faster - need to investigate)

# import pyboard

# class SerialCom():
#     def __init__(self):
#         self.active_com = False

#     def open_connection(self, port, baudrate):
#         if self.active_com == False:
#             self.com = pyboard.Pyboard(port, baudrate)
#             self.com.enter_raw_repl()

#     def close_connection(self):
#         pass

#     def __del__(self):
#         pass
