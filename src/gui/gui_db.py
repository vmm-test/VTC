""" 
    gui_db.py

    Three classes - each for one table in database

"""


from datetime import datetime
from sqlalchemy.orm import column_property
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, Float, String, DateTime, MetaData, Table
from sqlalchemy import func
Base = declarative_base()

def load_db(filename,measurement_no):
    engine = create_engine(filename)
    metadata=MetaData()
    Session = sessionmaker(bind=engine)
    session = Session()
    Querries=[]
    Names=["Reports","Records","Baselines"]
    Dict=[{},{},{}]
    Column_keys=[[],[],[]]
    for i in range(3):
        current_table=(Table(Names[i],metadata, Column('ID', Integer, primary_key=True), autoload=True, autoload_with=engine))
        query=session.query(current_table)
        query=query.filter_by(ID=measurement_no)
        Querries.append(query.all())
        current_dictionary=Dict[i]
        for idx, column in enumerate(current_table.c):
            Column_keys[i].append(column.name)
            key=(column.name).lower()
            value=Querries[i][0][idx]
            if value=="yes":
                value="ok"
            current_dictionary[key]=value
    return(Querries[0],Querries[1],Querries[2],Dict[0],Dict[1],Dict[2])

def search_db(filename, usr_input, chip_id, date, measurement_no):
            engine = create_engine(filename)
            metadata=MetaData()
            vmm_table = Table('Reports',metadata, Column('ID', Integer, primary_key=True), autoload=True, autoload_with=engine)
            Session = sessionmaker(bind=engine)
            session = Session()
            query=session.query(vmm_table)
            if usr_input[1]:
                query=query.filter_by(MEAS_DESCR=usr_input[0])
            if chip_id[1]:
                query=query.filter_by(CHIP_ID=chip_id[0])
            if date[1]:
                query=query.filter(func.DATE(Column('CREATE_DATE'))==date[0])
            if measurement_no[1]:
                query=query.filter_by(ID=measurement_no[0])
            final = query.all()
            return(final)

def delete_db(filename,measurement_no):
    engine = create_engine(filename)
    metadata=MetaData()
    Tables = [Table() for i in range(3)]
    Names=["Reports","Records","Baselines"]
    for i in range(3):
        current_table=Table(Names[i],metadata, Column('ID', Integer, primary_key=True), autoload=True, autoload_with=engine)
        d = current_table.delete().where(current_table.c.ID==measurement_no)
        engine.execute(d)


class Report(Base):
    __tablename__ = "REPORTS"

    empty = True

    id = Column("ID", Integer, primary_key=True, autoincrement=True)
    create_date = Column("CREATE_DATE", DateTime, nullable=False)
    user_input = Column("MEAS_DESCR", String)
    chip_id = Column("CHIP_ID", String)
    status = Column("STATUS", String)
    baseline_vmm0 = Column("BASELINE_VMM0", String)
    baseline_vmm1 = Column("BASELINE_VMM1", String)
    num_bad_ch_vmm0 = Column("NUM_BAD_CH_VMM0", Integer)
    num_bad_ch_vmm1 = Column("NUM_BAD_CH_VMM1", Integer)    
    bad_channels = Column("BAD_CHANNELS", String)
    # error_description = Column("ERROR_DESCRIPTION", String)
    power_errors = Column("POWER_ERRORS", String)
    i2c_errors = Column("I2C_ERRORS", String)
    hdmi_errors = Column("HDMI_ERRORS", String)
  
    def get_id(self):
        return self.id
        
    def __repr__(self):
        return "Reports ({} {})".format(self.id, self.create_date)
  
    def fill_user_input(self, usr_input):
        setattr(self, "user_input", usr_input)
        
    def fill_chip_id(self, chip_id):
        setattr(self, "chip_id", chip_id)
        self.empty = False

    def fill_data(self, data):
        # Report
        # report_keys = ["status","baseline_vmm0", "baseline_vmm1", "num_bad_ch_vmm0", "num_bad_ch_vmm1","bad_channels","error_description"]
        report_keys = ["create_date","status","baseline_vmm0", "baseline_vmm1", "num_bad_ch_vmm0", "num_bad_ch_vmm1", "bad_channels", "power_erros", "i2c_errors", "hmdi_errors"]
        for key in report_keys:
            if key in data:
                # set attribute of an Record object by key
                setattr(self, key, data[key])
                self.empty = False
    def save(self,filename,record_table,baseline_table):
        try:
            engine = create_engine(filename)
            # Create database table(s) if not exists
            Base.metadata.create_all(engine)

            # Prepare for writing
            Session = sessionmaker(bind=engine)
            session = Session()
            session.add(self)
            session.add(record_table)
            session.add(baseline_table)
            session.commit()
            # Review
            print(self)
        except Exception as e:
            print("Problem writing into the database")
            print(e)




class Record(Base):
    __tablename__ = "RECORDS"

    empty = True

    id = Column("ID", Integer, primary_key=True)

    # I2C
    # chip_address = Column("CHIP_ADDRESS", String)
    fpga = Column("FPGA", String)
    eeprom = Column("EEPROM", String)
    uadc1 = Column("UADC1", String)
    uadc2 = Column("UADC2", String)

    # PINS
    pin1u = Column("PIN1U", Float)
    pin1r = Column("PIN1R", Float)
    pin1r_units = Column("PIN1R_UNITS", String)

    pin2u = Column("PIN2U", Float)
    pin2r = Column("PIN2R", Float)
    pin2r_units = Column("PIN2R_UNITS", String)

    pin3u = Column("PIN3U", Float)
    pin3r = Column("PIN3R", Float)
    pin3r_units = Column("PIN3R_UNITS", String)

    pin4u = Column("PIN4U", Float)
    pin4r = Column("PIN4R", Float)
    pin4r_units = Column("PIN4R_UNITS", String)

    pin5u = Column("PIN5U", Float)
    pin5r = Column("PIN5R", Float)
    pin5r_units = Column("PIN5R_UNITS", String)

    pin6u = Column("PIN6U", Float)
    pin6r = Column("PIN6R", Float)
    pin6r_units = Column("PIN6R_UNITS", String)

    pin7u = Column("PIN7U", Float)
    pin7r = Column("PIN7R", Float)
    pin7r_units = Column("PIN7R_UNITS", String)

    pin9u = Column("PIN9U", Float)
    pin9r = Column("PIN9R", Float)
    pin9r_units = Column("PIN9R_UNITS", String)

    pin10u = Column("PIN10U", Float)
    pin10r = Column("PIN10R", Float)
    pin10r_units = Column("PIN10R_UNITS", String)

    pin11u = Column("PIN11U", Float)
    pin11r = Column("PIN11R", Float)
    pin11r_units = Column("PIN11R_UNITS", String)

    pin12u = Column("PIN12U", Float)
    pin12r = Column("PIN12R", Float)
    pin12r_units = Column("PIN12R_UNITS", String)

    pin13u = Column("PIN13U", Float)
    pin13r = Column("PIN13R", Float)
    pin13r_units = Column("PIN13R_UNITS", String)

    pin14u = Column("PIN14U", Float)
    pin14r = Column("PIN14R", Float)
    pin14r_units = Column("PIN14R_UNITS", String)

    pin17u = Column("PIN17U", Float)
    pin17r = Column("PIN17R", Float)
    pin17r_units = Column("PIN17R_UNITS", String)

    pin18u = Column("PIN18U", Float)
    pin18r = Column("PIN18R", Float)
    pin18r_units = Column("PIN18R_UNITS", String)

    pin19u = Column("PIN19U", Float)
    pin19r = Column("PIN19R", Float)
    pin19r_units = Column("PIN19R_UNITS", String)

    auxu = Column("AUXU", Float)
    auxr = Column("AUXR", Float)
    auxr_units = Column("AUXR_UNITS", String)

    # Power
    p1 = Column("P1", Float)
    p1a = Column("P1A", Float)
    p1b = Column("P1B", Float)
    p2 = Column("P2", Float)
    p2a = Column("P2A", Float)
    p2b = Column("P2B", Float)
    p1vmm = Column("P1VMM", Float)
    p2vmm = Column("P2VMM", Float)
  
    def __repr__(self):
        return "Records ({} {})".format(self.id, self.create_date)
  
    def fill_data(self, id, data):
        """ Fill record with (partial) data coming from VMM chip

            Parameters:
            -----------
            data (dict) - data from VMM (p1p2id, mux1, mux3 scripts)
        """
        #
        # Pins
        #
        def get_pin_values(pin):
            """ Helper function to extract pin data from VMM readings"""
            u = data[pin].get("U", None)
            r = data[pin].get("R", None)
            r_units = data[pin].get("Runits", None)
            return u, r, r_units

        def set_pin_values(pin, u, r, r_units):
            """ Helper function to set pin values into Record object"""
            setattr(self, "{pin}u".format(pin=pin), u)
            setattr(self, "{pin}r".format(pin=pin), r)
            setattr(self, "{pin}r_units".format(pin=pin), r_units)

            self.empty = False

        # Fill pin data
        for i in range(1, 20):
          if (i!=8 and i!=15 and i!=16):
            pin = "pin{i}".format(i=i)
            if pin in data:
                u, r, r_units = get_pin_values(pin)
                set_pin_values(pin, u, r, r_units)

        # Exceptional pins
        if "aux" in data:
            u, r, r_units = get_pin_values("aux")
            set_pin_values("aux", u, r, r_units)

        # Power
        power_keys = ["p1", "p1a", "p1b", "p2", "p2a", "p2b", "p1vmm", "p2vmm"]

        # I2C
        i2c_keys = ["fpga", "eeprom", "uadc1", "uadc2"]

        for key in i2c_keys:
            if key in data:
                    setattr(self, key, "yes")
                    if data[key]=="NULL":
                        setattr(self, key, "-")
        for key in power_keys:
            if key in data:
                # set attribute of an Record object by key
                setattr(self, key, data[key])
                self.empty = False


class Baseline(Base):
    __tablename__ = "BASELINES"

    empty = True

    id = Column("ID", Integer, primary_key=True)
    
    ch00_0 = Column("CH00_0", Float)
    ch01_0 = Column("CH01_0", Float)
    ch02_0 = Column("CH02_0", Float)
    ch03_0 = Column("CH03_0", Float)
    ch04_0 = Column("CH04_0", Float)
    ch05_0 = Column("CH05_0", Float)
    ch06_0 = Column("CH06_0", Float)
    ch07_0 = Column("CH07_0", Float) 
    ch08_0 = Column("CH08_0", Float)
    ch09_0 = Column("CH09_0", Float)

    ch10_0 = Column("CH10_0", Float)
    ch11_0 = Column("CH11_0", Float)
    ch12_0 = Column("CH12_0", Float)
    ch13_0 = Column("CH13_0", Float)
    ch14_0 = Column("CH14_0", Float)
    ch15_0 = Column("CH15_0", Float)
    ch16_0 = Column("CH16_0", Float)
    ch17_0 = Column("CH17_0", Float) 
    ch18_0 = Column("CH18_0", Float)
    ch19_0 = Column("CH19_0", Float)
           
    ch20_0 = Column("CH20_0", Float)
    ch21_0 = Column("CH21_0", Float)
    ch22_0 = Column("CH22_0", Float)
    ch23_0 = Column("CH23_0", Float)
    ch24_0 = Column("CH24_0", Float)
    ch25_0 = Column("CH25_0", Float)
    ch26_0 = Column("CH26_0", Float)
    ch27_0 = Column("CH27_0", Float) 
    ch28_0 = Column("CH28_0", Float)
    ch29_0 = Column("CH29_0", Float)

    ch30_0 = Column("CH30_0", Float)
    ch31_0 = Column("CH31_0", Float)
    ch32_0 = Column("CH32_0", Float)
    ch33_0 = Column("CH33_0", Float)
    ch34_0 = Column("CH34_0", Float)
    ch35_0 = Column("CH35_0", Float)
    ch36_0 = Column("CH36_0", Float)
    ch37_0 = Column("CH37_0", Float) 
    ch38_0 = Column("CH38_0", Float)
    ch39_0 = Column("CH39_0", Float)

    ch40_0 = Column("CH40_0", Float)
    ch41_0 = Column("CH41_0", Float)
    ch42_0 = Column("CH42_0", Float)
    ch43_0 = Column("CH43_0", Float)
    ch44_0 = Column("CH44_0", Float)
    ch45_0 = Column("CH45_0", Float)
    ch46_0 = Column("CH46_0", Float)
    ch47_0 = Column("CH47_0", Float) 
    ch48_0 = Column("CH48_0", Float)
    ch49_0 = Column("CH49_0", Float)

    ch50_0 = Column("CH50_0", Float)
    ch51_0 = Column("CH51_0", Float)
    ch52_0 = Column("CH52_0", Float)
    ch53_0 = Column("CH53_0", Float)
    ch54_0 = Column("CH54_0", Float)
    ch55_0 = Column("CH55_0", Float)
    ch56_0 = Column("CH56_0", Float)
    ch57_0 = Column("CH57_0", Float) 
    ch58_0 = Column("CH58_0", Float)
    ch59_0 = Column("CH59_0", Float)
           
    ch60_0 = Column("CH60_0", Float)
    ch61_0 = Column("CH61_0", Float)
    ch62_0 = Column("CH62_0", Float)
    ch63_0 = Column("CH63_0", Float)

    ch00_1 = Column("CH00_1", Float)
    ch01_1 = Column("CH01_1", Float)
    ch02_1 = Column("CH02_1", Float)
    ch03_1 = Column("CH03_1", Float)
    ch04_1 = Column("CH04_1", Float)
    ch05_1 = Column("CH05_1", Float)
    ch06_1 = Column("CH06_1", Float)
    ch07_1 = Column("CH07_1", Float) 
    ch08_1 = Column("CH08_1", Float)
    ch09_1 = Column("CH09_1", Float)

    ch10_1 = Column("CH10_1", Float)
    ch11_1 = Column("CH11_1", Float)
    ch12_1 = Column("CH12_1", Float)
    ch13_1 = Column("CH13_1", Float)
    ch14_1 = Column("CH14_1", Float)
    ch15_1 = Column("CH15_1", Float)
    ch16_1 = Column("CH16_1", Float)
    ch17_1 = Column("CH17_1", Float) 
    ch18_1 = Column("CH18_1", Float)
    ch19_1 = Column("CH19_1", Float)
           
    ch20_1 = Column("CH20_1", Float)
    ch21_1 = Column("CH21_1", Float)
    ch22_1 = Column("CH22_1", Float)
    ch23_1 = Column("CH23_1", Float)
    ch24_1 = Column("CH24_1", Float)
    ch25_1 = Column("CH25_1", Float)
    ch26_1 = Column("CH26_1", Float)
    ch27_1 = Column("CH27_1", Float) 
    ch28_1 = Column("CH28_1", Float)
    ch29_1 = Column("CH29_1", Float)

    ch30_1 = Column("CH30_1", Float)
    ch31_1 = Column("CH31_1", Float)
    ch32_1 = Column("CH32_1", Float)
    ch33_1 = Column("CH33_1", Float)
    ch34_1 = Column("CH34_1", Float)
    ch35_1 = Column("CH35_1", Float)
    ch36_1 = Column("CH36_1", Float)
    ch37_1 = Column("CH37_1", Float) 
    ch38_1 = Column("CH38_1", Float)
    ch39_1 = Column("CH39_1", Float)

    ch40_1 = Column("CH40_1", Float)
    ch41_1 = Column("CH41_1", Float)
    ch42_1 = Column("CH42_1", Float)
    ch43_1 = Column("CH43_1", Float)
    ch44_1 = Column("CH44_1", Float)
    ch45_1 = Column("CH45_1", Float)
    ch46_1 = Column("CH46_1", Float)
    ch47_1 = Column("CH47_1", Float) 
    ch48_1 = Column("CH48_1", Float)
    ch49_1 = Column("CH49_1", Float)

    ch50_1 = Column("CH50_1", Float)
    ch51_1 = Column("CH51_1", Float)
    ch52_1 = Column("CH52_1", Float)
    ch53_1 = Column("CH53_1", Float)
    ch54_1 = Column("CH54_1", Float)
    ch55_1 = Column("CH55_1", Float)
    ch56_1 = Column("CH56_1", Float)
    ch57_1 = Column("CH57_1", Float) 
    ch58_1 = Column("CH58_1", Float)
    ch59_1 = Column("CH59_1", Float)
           
    ch60_1 = Column("CH60_1", Float)
    ch61_1 = Column("CH61_1", Float)
    ch62_1 = Column("CH62_1", Float)
    ch63_1 = Column("CH63_1", Float)
    
    def __repr__(self):
        return "Baselines ({} {})".format(self.id)
        
    def fill_data(self, id, data):
        setattr(self, "id", id)
        
        for i in range(64):
            print(data[i])
            print(i)
            setattr(self, "ch{:02d}_0".format(i), data[i][0][0])
            setattr(self, "ch{:02d}_1".format(i), data[i][1][0])
