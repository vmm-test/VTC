""" 
    gui_db_window.py

    Window displaying widgets for chosing database file and saving into it or
    searching in it and then loading/deleting items from it.

"""

import tkinter as tk
from tkinter import *
from tkinter import filedialog
import tkinter.ttk as ttk
from tkinter import messagebox as mb
import os.path

import gui_db
import meas_result

# global variable for remembering the database filename between opening and closing this window
# (during one run of the whole program)
db_filename_str = ""

class DbMng(tk.Toplevel):
    def __init__(self, *args, hybrid_name, report_table, record_table, baseline_table, gui_update):
        # initialization of the window, class extends ordinary Tkinter Toplevel
        super().__init__(*args)
        self.geometry("950x1100")
        self.title("Database management")
        self.gui_update = gui_update

        self.db_filename = tk.StringVar()

        ########## Window layout ###########

        ####### Two main frames divided by horizontal Separator #######
        dbFileFrame = Frame(self, width=700, height=100)
        dbFileFrame.grid(row=1, column=1, sticky=W)
        ttk.Separator(self, orient="horizontal").grid(row=2,column=1, sticky=EW)
        recordsFrame = Frame(self, width= 1000, height=1100)
        recordsFrame.grid(row=3, column=1, sticky=W)

        ####### First frame containing Entry for chosing file #######
        Label(dbFileFrame, text="Working with database file:", font=('TkDefaultFont', 15)).pack(side=LEFT,padx=30,pady=40)
        global db_filename_str
        self.db_filename.set(db_filename_str)
        ttk.Entry(dbFileFrame, textvariable=self.db_filename, width=60).pack(side=LEFT)

        def choose_db():
            self.db_filename.set(filedialog.asksaveasfilename(parent=self, filetypes=[("Sqlite files", ".sqlite"), ("All files", "*")]))
        ttk.Button(dbFileFrame, text="Browse", command=choose_db).pack(side=LEFT)

        ####### Left portion of the second frame - database search #######
        col1Frame1_db = Frame(recordsFrame, height=1050, width=600)
        col1Frame1_db.pack_propagate(0)
        col1Frame1_db.pack(side=LEFT, anchor=N, padx=10)
        frameLabelcol1_db = Label(col1Frame1_db, text="Search database")
        frameLabelcol1_db.pack(side=TOP,padx=5, pady=5)
        def search(ents):
            filename=self.db_filename.get() #ents['Search database'].get()
            if os.path.isfile(filename):
                global db_filename_str
                db_filename_str = filename
                set_search = lambda key: (ents[key].get(), True if self.filters[key].get() == True else False)
                
                date= set_search("Measurement date")
                usr_input = set_search("Measurement (user) description")
                chip_id = set_search("Chip id")
                measurement_no = set_search("Measurement no.")

                result=gui_db.search_db("sqlite:///"+filename,usr_input,chip_id,date,measurement_no)
                update_db_search(result,"sqlite:///"+filename)
            else:
                update_db_search(None, None, succ=False)
        search_fields=(["Measurement (user) description","Chip id","Measurement date", "Measurement no."])
        entries = {}
        self.filters = {}

        def filter_editable(key):
            if self.filters[key].get() == True:
                entries[key].state(('!readonly',))
            else:
                entries[key].state(('readonly',))

        for search_field in search_fields:
            row = Frame(col1Frame1_db)
            lab = Label(row, width=30, text=search_field+": ", anchor='w')
            ent = ttk.Entry(row)
            fil_var = BooleanVar(value=False)
            fil = ttk.Checkbutton(row, variable=fil_var, onvalue=True,offvalue=False, command=lambda key=search_field: filter_editable(key))

            if search_field=="Measurement date":
                ent.insert(0, "XXXX-XX-XX")
            else:
                ent.insert(0, "0")
            row.pack(side=TOP, 
                fill=BOTH, 
                padx=5, 
                pady=5)
            lab.pack(side=LEFT)
            ent.pack(side=LEFT, 
                expand=YES, 
                fill=X)
            fil.pack(side=LEFT)
            entries[search_field] = ent
            self.filters[search_field] = fil_var
            filter_editable(search_field)
        
        b = ttk.Button(col1Frame1_db, text='Search',
                command=(lambda e=entries: search(e)))
        b.pack(side=TOP, padx=5, pady=5)

        frameLabelcol_records = Label(col1Frame1_db, text="Records found")
        frameLabelcol_records.pack(side=TOP,padx=5, pady=5)
        self.results_db_frame = Frame(col1Frame1_db, bd=1, relief=SOLID)
        self.results_db_frame.pack(fill=BOTH)
        Label(self.results_db_frame, text='No data').pack()

        ####### Vertical separator and right portion of the second frame for saving into database #######
        sepFrame = Frame(recordsFrame, height=500, width=30,bd=0,relief=SOLID)
        sepFrame.pack(side=LEFT,anchor=N, pady=20, fill=Y)
        ttk.Separator(sepFrame, orient=VERTICAL).pack(fill=Y,expand=1)
        colFrame2_db = Frame(recordsFrame, height=1000, width=150)
        colFrame2_db.pack(side=LEFT, anchor=N, padx=30)

        connCanvas_db = Canvas(colFrame2_db, height=1000, width=150, highlightthickness=0)
        connCanvas_db.pack()

        connY1_db = 30
        col2Button1Oval_db = connCanvas_db.create_oval(30, connY1_db, 130, connY1_db+100, fill="#dfffd4", outline="#32cd32", width=2, tag="Button1_db")
        col2Button1Text_db = connCanvas_db.create_text(80, connY1_db+50, text="     Save\ncurrent record")
        connCanvas_db.tag_bind(col2Button1Oval_db, "<Button-1>", lambda ev, win=self: col1Button1_db(ev, win))
        connCanvas_db.tag_bind(col2Button1Text_db, "<Button-1>", lambda ev, win=self: col1Button1_db(ev, win))


        ############ Save to DB function invoked by the green button ###########
        def col1Button1_db(event, par_window):
            print("Clicked Save to DB")

            # Change button collor
            oval = connCanvas_db.find_withtag("Button1_db")
            # connCanvas_db.itemconfig(oval, fill="#ffc588") # doesn't work for some reason
            connCanvas_db.update()
            def saveDatabase(ents, window):
                # directory = filedialog.asksaveasfilename()
                directory = self.db_filename.get()
                if directory == "":
                    mb.showwarning("No path", "Fill in path to the database file first before saving.", parent=self)
                    return False
                try:
                    report_table.fill_user_input(ents['Measurement (user) description:'].get())
                    report_table.save("sqlite:///"+directory,record_table,baseline_table)
                    return True
                except Exception as e:
                    print(e)
                    print("Problem writing into database")
                    mb.showerror("Save fail", "Saving to database failed, verify whether you've chosen valid path to database file.")
                    return False

            # Check if report is filled with data from VMM
            if report_table.empty:
                mb.showwarning("No Data", "Press J2 button to read data from VMM")
                par_window.destroy()

            # Check if record has ID (was already saved into database)
            elif report_table.id:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                mb.showwarning("Already saved", "Record was already saved into database")
                par_window.destroy()
                
            # Report is not empty and not yet saved into database. Save it!
            else:
                if saveDatabase({"Measurement (user) description:": hybrid_name},None) == True:
                    global db_filename_str
                    db_filename_str = self.db_filename.get()
                    par_window.destroy()
                else:
                    self.lift()

            # Change button collor back 
            # connCanvas_db.itemconfig(oval, fill="#88c2ff") # doesn't work for some reason
            connCanvas_db.update()
            # par_window.destroy()

        ############ Function displaying search results and Load and Delete buttons ############
        # If search was successful, calls update to draw the results, otherwise errors out.
        # 
        # Load: loads the item from database, preprocesses it and saves it to class defined in meas_result.py
        # Delete: deletes the record from database, calls update again
        # update: draws the results and buttons

        def update_db_search(records, filename, succ=True):
            def Load(measurement_no):
                reports,records,baselines,reports_dict,records_dict,baselines_dict=gui_db.load_db(filename,measurement_no)

                # power and i2c
                aux_i2c_records = ["fpga","eeprom","uadc1","uadc2","p1","p2","p1a","p1b","p2a","p2b","p1vmm","p2vmm"]
                meas_result.data.power_i2c = {k:v for k,v in records_dict.items() if k in aux_i2c_records}
                meas_result.data.power_i2c.update({"chip_id": reports_dict["chip_id"]})
                if meas_result.data.power_i2c.get("chip_id", "?") not in ("?", None):
                    meas_result.data.power_i2c["id_chip"] = "ok"
                
                # baselines
                aux_baselines = []
                baselines  = baselines[0][1:]
                for i in range(64):
                    aux_baselines.append([[baselines[i], 0], [baselines[i+64], 0]])
                meas_result.data.baselines = aux_baselines

                # HDMI pins
                def db_to_mux(array):
                    mux_array = {}
                    for i in range(1,19):
                        key = "pin"+str(i)
                        if (i==8 or i==15 or i==16):
                            continue
                        else:
                            value = {'Runits':array[key+"r_units"],'U':array[key+"u"],'R':array[key+"r"]}
                            mux_array[key]=value
                    return mux_array
                meas_result.data.mux = db_to_mux(records_dict)

                # report
                meas_result.data.report = {k:v for k,v in reports_dict.items() if k != "id"}

                # signals to update the gui
                self.gui_update.set(not self.gui_update.get())

            def Delete(tree):
                if mb.askokcancel ('Delete Record','Are you sure you want to delete the record?', parent=self):
                    treeitem = tree.focus()
                    measurement_no = tree.item(treeitem).get('text')
                    gui_db.delete_db(filename,measurement_no)
                    tree.delete(treeitem)

            def update(records):
                self.results_db_frame.destroy()
                self.results_db_frame = Frame(col1Frame1_db)
                self.results_db_frame.pack(fill=BOTH)#

                rows_db = []
                for row in records:
                    rows_db.append([row.ID,row.CREATE_DATE,row.MEAS_DESCR, row.CHIP_ID])

                cols = ['#0', 'CREATE_DATE', 'MEAS_DESCR', 'CHIP_ID']
                headings = ['ID', 'Date', 'Description', 'Chip ID']
                col_widths = [50, 150, 150, 250]

                search_list = ttk.Treeview(self.results_db_frame, columns=cols[1:], height=30)

                for i in range(len(cols)):
                    search_list.column(cols[i], width=col_widths[i], anchor='center')
                    search_list.heading(cols[i], text=headings[i])
                search_list['selectmode'] = 'browse'

                for i in rows_db:
                    search_list.insert('','end',text=i[0],values=i[1:])

                for col in cols:
                    search_list.column(col, stretch='false')

                search_list.grid(column=0, row=1, columnspan=3, sticky=(W,E))
                ttk.Button(self.results_db_frame, text='Load', command=lambda:Load(search_list.item(search_list.focus()).get('text'))).grid(column=1, row=0, pady=10)
                ttk.Button(self.results_db_frame, text='Delete', command=lambda:Delete(search_list)).grid(column=2, row=0, pady=10)
                self.results_db_frame.columnconfigure(0, weight=1)

                scroll = ttk.Scrollbar(self.results_db_frame, orient=VERTICAL, command=search_list.yview)
                search_list.configure(yscrollcommand=scroll.set)
                scroll.grid(column=3,row=1,sticky=(N,S))

            if succ:
                update(records)
            else:
                self.results_db_frame.destroy()
                self.results_db_frame = Frame(col1Frame1_db, bd=1, relief=SOLID)
                self.results_db_frame.pack(fill=BOTH)#
                Label(self.results_db_frame, text="The database file does not exist!").pack()