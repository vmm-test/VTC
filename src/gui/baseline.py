""" 
    baseline.py

    Class for measuring baseline values. 
    VMM configuration is defined in constructor.

    Main methods for user are set_channel, measure, measure_allchan and get_avg
    First 3 manipulates mainly properties, last one only returns values

    For computing baselines of all channels it is enough to create instance, call measure_allchan
    and finally get_avg to compute averages (of multiple measurements of the same channel - configurable)

    "vmms" parameter sets which VMM is measured:
        1 - VMM1
        2 - VMM2
        3 - both

    

"""

import json
import time
import statistics

import gui_serial
import gui_cfg


class Baseline():
    def __init__(self, port, channel=0):

        self.port = port

        # some config defaults, can be changed here (or at each instance)
        self.cfg = gui_cfg.VMMconfig()
        self.cfg.mod('sbfm', 1)
        self.cfg.mod('scmx', 1)
        self.cfg.mod('st', 3)
        self.cfg.mod('sbip', 1)
        self.cfg.mod('sdcks', 1)
        self.cfg.mod('sdt', 300)
        self.cfg.mod('sdp10', 500)
        self.cfg.mod('sg', 7)

        # set channel
        self.channel = channel
        self.set_channel(channel)

        self.vals = []
        for _ in range(64):
            self.vals.append(None)

    def set_channel(self, channel, to_pyboard=False):
        if channel < 0 or channel > 63:
            raise ValueError("Permited channel is from 0 to 63")

        self.cfg.mod('sm', channel)

        # if there are some specific channel settings (so far example)
        # self.cfg.mod('', 0, channel) # reset previous chan
        # self.cfg.mod('', 1, channel) # set chosen chan

        if to_pyboard == True and channel != self.channel:
            gui_serial.serial_write('vmm_cfg.cfg[0:2] = {}'.format(self.cfg.get()[0:2]), self.port, timeout=0)
            gui_serial.serial_write('vmm_baseline.change_channel()', self.port, timeout=0)
            time.sleep(0.010)

        self.channel = channel

    def _setup(self, vmms):
        print(self.cfg.send(self.port))
        print(gui_serial.serial_write('vmm_baseline.setup({})'.format(vmms), self.port, timeout=1))
        time.sleep(0.001)

    def _finish(self, vmms):
        gui_serial.serial_write('vmm_baseline.finish({})'.format(vmms), self.port, timeout=0)

    def _single_meas(self, vmms):
        ret = gui_serial.serial_write('vmm_baseline.measure({})'.format(vmms), self.port, timeout=5)
        print(ret) # for debug
        # this is still very sensitive on what's in the buffers - needs different serial control
        while True:
            try:
                ret = json.loads(ret[0])
                break
            except json.decoder.JSONDecodeError:
                ret = ret[1:]
        return ret

    def measure(self, count, vmms, set_fin=True, auto_resolution=False, save_res=True):
        if vmms < 1 or vmms > 3:
            raise ValueError("vmms input can be only from 1 to 3 (1 - VMM1, 2 - VMM2, 3 - both")

        if set_fin == True:
            self._setup(vmms) # for speedup the setup in the pyboard could 
        
        results1 = []
        results2 = []

        # auto_resolution = False
        if auto_resolution == True:
            adjust = [False, False]
            aux_res = self._single_meas(vmms)
            adjust = [x > 250 for x in aux_res]

            if any(adjust) == True:
                gui_serial.serial_write('vmm_baseline.adjust_range(1, {})'.format(adjust[0]*1 + adjust[1]*2), self.port, timeout=0)
                print(f'adjust {adjust[0]} {adjust[1]}')
                time.sleep(0.003)

            if all(adjust) == False:
                if vmms == 1 or vmms == 3:
                    results1.append(aux_res[0])
                if vmms > 1:
                    results2.append(aux_res[1])
                count = count - 1

        for _ in range(count):
            ret = self._single_meas(vmms)

            # ret = json.loads(ret[0])
            if vmms == 1 or vmms == 3:
                results1.append(ret[0])
            if vmms > 1:
                results2.append(ret[1])

        if not (vmms == 1 or vmms == 3):
            results1.append(None)

        if not (vmms > 1):
            results2.append(None)

        if auto_resolution == True and any(adjust) == True:
            gui_serial.serial_write('vmm_baseline.adjust_range(0, {})'.format(adjust[0]*1 + adjust[1]*2), self.port, timeout=0)
            time.sleep(0.001)

        if set_fin == True:
            self._finish(vmms)

        if save_res == True:
            self.vals[self.channel] = (results1, results2)

        return (results1, results2)


    def measure_allchan(self, count, vmms, auto_resolution=False, save_res=True):
        res = []
        self._setup(vmms)
        time.sleep(0.100)
        for i in range(64):
            self.set_channel(i, to_pyboard=True)
            res.append(self.measure(count, vmms, False, auto_resolution, save_res))
        self._finish(vmms)
        return res

    def get_avg(self, chan):
        if chan == 'all':
            picked_chan = range(64)
        elif chan >= 0 and chan < 64:
            picked_chan = [chan]
        else:
            raise ValueError("Input channel values can be either 'all' for all channels or number between 0 and 63")

        res = []
        for i in picked_chan:
            aux = self.vals[i]
            if aux == None:
                res.append(None)
            else:
                res_chan = []
                for j in aux:
                    if j == None:
                        res_chan.append(None)
                    else:
                        res_chan.append((round(statistics.mean(j), 2), statistics.stdev(j)))
                res.append(res_chan)

        if len(res) == 1:
            return res[0]
        else:
            return res