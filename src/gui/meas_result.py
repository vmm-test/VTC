""" meas_result.py 

    Class for containing the results of the measurement for either saving to database or printing to label.
    It also has a method for creating the report (results overview) for database.

"""

import datetime

import thresholds
import label_print

class Measurement():
    def __init__(self):
        self.report = {}
        self.power_i2c = {}
        self.baselines = []
        self.mux = {}
        self.label = label_print.Label()

    def create_report(self):
        status_ok = True
        def verify_thresh(item, group, thresh):
            aux = group.get(item, "?")
            if aux != "?":
                return thresh(aux)
            else:
                return None

        # power errors
        errs = []
        for i in ("p1", "p1vmm", "p2", "p2vmm"):
            if verify_thresh(i, self.power_i2c, thresholds.power[i]) == False:
                status_ok = False
                if self.power_i2c.get(i, "?") < thresholds.power_vals[i][0]:
                    errs.append("{} low".format(i.upper()))
                else:
                    errs.append("{} high".format(i.upper()))
        self.report["power_errors"] = ",".join(errs)

        # I2C errors
        errs = []
        for i in ("fpga", "eeprom", "uadc1", "uadc2"):
            aux = self.power_i2c.get(i, "?")
            if not isinstance(aux, int):
                status_ok = False
                errs.append(i.upper())
        self.report["i2c_errors"] = ",".join(errs)

        # HDMI pins errors
        errs = []
        for i in self.mux:
            if not thresholds.j2[i](self.mux[i]["R"]):
                status_ok = False
                if i == "aux":
                    errs.append(i)
                elif i == "pin14":
                    errs.append("14/19")
                elif i != "pin19":
                    errs.append(i[3:])
        self.report["hdmi_errors"] = ",".join(errs)

        # baseline average and errors
        errs = []
        bn0, bn1 = 0, 0
        for n, i in enumerate(self.baselines):
            if not thresholds.baseline(i[0][0]):
                status_ok = False
                errs.append(n)
                bn0 += 1

            if not thresholds.baseline(i[1][0]):
                status_ok = False
                errs.append(n+64)
                bn1 += 1
        if len(self.baselines) != 0:
            self.report["baseline_vmm0"] = max([x[0][0] for x in self.baselines])
            self.report["baseline_vmm1"] = max([x[1][0] for x in self.baselines])
        self.report["num_bad_ch_vmm0"] = bn0
        self.report["num_bad_ch_vmm1"] = bn1
        self.report["bad_channels"] = ",".join([str(x) for x in sorted(errs)])
        
        # basic hybrid info and its status
        self.report["status"] = "OK" if status_ok == True else "ERROR"
        self.report["chip_id"] = self.power_i2c.get("chip_id", "?")
        self.report["create_date"] = datetime.datetime.utcnow().replace(microsecond=0)

# global class instance for holding measurement results
data = Measurement()