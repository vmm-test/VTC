"""
    thresholds.py

    All the thresholds determining whether the measured values is ok or not in one place.

"""

j2 = {
    "aux": lambda x: x > 1000,
    "pin1": lambda x: x > 1,
    "pin2": lambda x: x < 0.004,
    "pin3": lambda x: x > 1,
    "pin4": lambda x: x > 1,
    "pin5": lambda x: x < 0.004,
    "pin6": lambda x: x > 1,
    "pin7": lambda x: x > 1,

    "pin9": lambda x: x > 1,
    "pin10": lambda x: x > 1,
    "pin11": lambda x: x < 0.004,
    "pin12": lambda x: x > 1,
    "pin13": lambda x: x > 5,
    "pin14": lambda x: x > 100,

    "pin17": lambda x: x < 4,
    "pin18": lambda x: x > 4,
    "pin19": lambda x: x > 100
}

j2_str = {
    "aux": "(> 1000)",
    "pin1": "(> 1)",
    "pin2": "(< 0.004)",
    "pin3": "(> 1)",
    "pin4": "(> 1)",
    "pin5": "(< 0.004)",
    "pin6": "(> 1)",
    "pin7": "(> 1)",

    "pin9": "(> 1)",
    "pin10": "(> 1)",
    "pin11": "(< 0.004)",
    "pin12": "(> 1)",
    "pin13": "(> 5)",
    "pin14": "(> 100)",

    "pin17": "(< 4)",
    "pin18": "(> 4)",
    "pin19": "(> 100)"
}

# minimum and maximum values for currents and voltages
power_vals = {
    "p1" : (0, 0.3),
    "p2" : (1, 2),
    "p1vmm" : (2.7, 3.3),
    "p2vmm" : (1.5, 2.2)
}

power = {
    "p1": lambda x: x > power_vals["p1"][0] and x < power_vals["p1"][1],
    "p2": lambda x: x > power_vals["p2"][0] and x < power_vals["p2"][1],
    "p1vmm": lambda x: x > power_vals["p1vmm"][0] and x < power_vals["p1vmm"][1],
    "p2vmm": lambda x: x > power_vals["p2vmm"][0] and x < power_vals["p2vmm"][1]
}

def baseline(val):
    return (val > 150 and val < 190)

