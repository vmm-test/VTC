""" gui_cfg.py """

import array
import gui_serial

class VMMconfig:
    def __init__(self):
        # initialize array with 216 zeroed bytes (configuration has 1728 bits = 216 bytes)
        
        ###########################################
        # channel register
        ###########################################
        self.cfg_arr = array.array('B', [0] * 216)
        self.ch_tab = {}
        # sc,large sensor capacitance mode
        # ([0] <=200 pF , [1] >= 200 pF )
        self.ch_tab['sc'] = (0, 0, 1)
        
        # sl, leakage current disable [0=enabled]
        self.ch_tab['sl'] = (0, 1, 1)
        
        # st, 300fF test capacitor [1=enabled]
        self.ch_tab['st'] = (0, 2, 1)
        
        # sth, multiplies test capacitor by 10
        self.ch_tab['sth'] = (0, 3, 1)
        
        # sm, mask enable [1=enabled]
        self.ch_tab['sm'] = (0, 4, 1)
        
        # smx, channel monitor mode
        # ([0]analog output,[1]trimmed threshold))
        self.ch_tab['smx'] = (0, 5, 1)
        
        # sd0-sd4, trim threshold DAC, 1 mV step
        #([0:0] trim 0 V,[1:1] trim -29 mV )
        self.ch_tab['sd'] = (0, 6, 5)
        
        # sz010b,sz110b,sz210b,sz310b,sz410b,10-bitADCoﬀset subtract
        self.ch_tab['sz10b'] = (1, 3, 5)
        
        # sz08b,sz18b,sz28b,sz38b,8-bitADCoffset subtract
        self.ch_tab['sz08b'] = (2, 0, 4)
        
        # sz06b, sz16b, sz26b, 6-bit ADC oﬀset subtract
        self.ch_tab['sz06b'] = (2, 4, 3)
        
        
        ###########################################
        # global register 1
        ###########################################
        self.g1_tab = {}
        
        # sp, input charge polarity ([0] negative, [1] positive)
        self.g1_tab['sp'] = (0, 0, 1)
        
        # sdp,  disable-at-peak
        self.g1_tab['sdp'] = (0, 1, 1)
        
        # sbmx, routes analog monitor to PDO output
        self.g1_tab['sbmx'] = (0, 2, 1)
        
        # sbft, [1] enable  TDO
        self.g1_tab['sbft'] = (0, 3, 1)
        
        # sbfp, [1] enable  PDO
        self.g1_tab['sbfp'] = (0, 4, 1)
        
        # sbfm, [1] enable MO
        self.g1_tab['sbfm'] = (0, 5, 1)
        
        # slg,  leakage current disable ([0] enabled)
        self.g1_tab['slg'] = (0, 6, 1)
        
        # Common monitor: scmx, sm5-sm0 [0 000001 to 000100],
        # pulser DAC (after pulser switch), threshold DAC,
        # band-gap reference, temperature sensor)
        # channel monitor: scmx, sm5-sm0 [1 000000 to 111111],
        # channels 0 to 63
        self.g1_tab['sm'] = (0, 7, 6)
        self.g1_tab['scmx'] = (1, 5, 1)
        
        # sfa,  ART enable
        self.g1_tab['sfa'] = (1, 6, 1)
        
        # sfam, [0] timing at threshold, [1] timing at peak
        self.g1_tab['sfam'] = (1, 7, 1)
        
        # st1,st0 , peaktime (200, 100, 50, 25 ns )
        self.g1_tab['st'] = (2, 0, 2)
        
        # sfm, enable dynamic discharge AC coupling ([1] enable)
        self.g1_tab['sfm'] = (2, 2, 1)
        
        # sg2,sg1,sg0, gain (0.5, 1, 3, 4.5, 6, 9, 12, 16 mV/fC)
        self.g1_tab['sg'] = (2, 3, 3)
        
        # sng, neighbor (channel and chip) triggering enable        
        self.g1_tab['sng'] = (2, 6, 1)
        
        # stot,  timing outputs control 1 (s6b must be disabled)
        # - stpp,stot[00,01,10,11]: TtP,ToT,PtP,PtT
        # - TtP: threshold-to-peak
        # - ToT: time-over-threshold
        # - PtP: pulse-at-peak (10ns) (not available with s10b)
        # - PtT: peak-to-threshold (not available with s10b)
        self.g1_tab['stot'] = (2, 7, 1)
        
        # sttt, enables direct-output logic (timing and s6b)
        self.g1_tab['sttt'] = (3, 0, 1)
        
        # ssh, enables sub-hysteresis discrimination
        self.g1_tab['ssh'] = (3, 1, 1)
        
        # stc1,stc0 , TAC slope adjustment (60, 100,350,650 ns )
        self.g1_tab['stc'] = (3, 2, 2)
        
        # sdt9-sdt0 , coarse threshold DAC
        self.g1_tab['sdt'] = (3, 4, 10)
        
        # sdp9-sdp0 , test pulse DAC
        self.g1_tab['sdp10'] = (4, 6, 10)
        
        # sc010b,sc110b,10-bitADC conv.time (subtracts 60 ns)
        self.g1_tab['sc10b'] = (6, 0, 2)
        
        # sc08b,sc18b, 8-bit ADC conv.time ( subtracts 60 ns)
        self.g1_tab['sc8b'] = (6, 2, 2)
        
        # sc06b, sc16b, sc26b ,  6-bit ADC conversion time
        self.g1_tab['sc6b'] = (6, 4, 3)
        
        # s8b, 8-bit ADC conversion mode
        self.g1_tab['s8b'] = (6, 7, 1)
        
        # s6b, enables 6-bit ADC (requires sttt enabled)
        self.g1_tab['s6b'] = (7, 0, 1)
        
        # s10b,enables high resolution ADCs(10/8-bit ADC enable)
        self.g1_tab['s10b'] = (7, 1, 1)
        
        # sdcks,  dual clock edge serialized data enable
        self.g1_tab['sdcks'] = (7, 2, 1)
        
        # sdcka, dual clock edge serialized ART enable
        self.g1_tab['sdcka'] = (7, 3, 1)
        
        # sdck6b, dual clock edge serialized 6-bit enable
        self.g1_tab['sdck6b'] = (7, 4, 1)
        
        # sdrv,tristates analog output switch in analog mode
        self.g1_tab['sdrv'] = (7, 5, 1)
        
        # stpp, timing outputs control 2
        self.g1_tab['stpp'] = (7, 6, 1)
        
        # res00 reserved bit ( for what ?)
        self.g1_tab['res00'] = (7, 7, 1)
        
        # reso,res1,res2,res4,  reserved bits ( for what ?)
        self.g1_tab['res0'] = (8, 0, 1)
        self.g1_tab['res1'] = (8, 1, 1)
        self.g1_tab['res2'] = (8, 2, 1)
        self.g1_tab['res3'] = (8, 3, 1)
        
        # slvs, enables direct output IOs (increased current !)
        self.g1_tab['slvs'] = (8, 4, 1)
        
        # s32,  skips channels 16-47, makes 15 and 48 neighbors
        self.g1_tab['s32'] = (8, 5, 1)
        
        # stcr, enables auto-reset
        # (at the end of the ramp, if no stop occurs)
        self.g1_tab['stcr'] = (8, 6, 1)
        
        # ssart, enables ART ﬂag synchronization(trail to next )
        self.g1_tab['ssart'] = (8, 7, 1)
        
        # srec,  enables fast recovery from high charge
        self.g1_tab['srec'] = (9, 0, 1)
        
        # stlc, enables mild tail cancellation
        # (when enabled, overrides sbip)
        # set stlc or sbip as workaround for baseline problem
        self.g1_tab['stlc'] = (9, 1, 1)
        
        # sbip,   enables bipolar shape
        # set stlc or sbip  as workaround for baseline problem
        self.g1_tab['sbip'] = (9, 2, 1)
        
        # srat,   enables timing ramp at threshold
        self.g1_tab['srat'] = (9, 3, 1)
        
        # sfrst, enables fast reset at 6-b completion
        self.g1_tab['sfrst'] = (9, 4, 1)
        
        # slvsbc,  enable slvs 100 Ω termination on ckbc
        self.g1_tab['slvsbc'] = (9, 5, 1)
        
        # slvstp,  enable slvs 100 Ω termination on cktp
        self.g1_tab['slvstp'] = (9, 6, 1)
        
        # slvstk, enable slvs 100 Ω termination on cktk
        self.g1_tab['slvstk'] = (9, 7, 1)
        
        # slvsdt, enable slvs 100 Ω termination on ckdt
        self.g1_tab['slvsdt'] = (10, 0, 1)
        
        # slvsart, enable slvs 100 Ω termination on ckart
        self.g1_tab['slvsart'] = (10, 1, 1)
        
        # slvstki, enable slvs 100 Ω termination on cktki
        self.g1_tab['slvstki'] = (10, 2, 1)
        
        # slvsena, enable slvs 100 Ω termination on ckena
        self.g1_tab['slvsena'] = (10, 3, 1)
        
        # slvs6b, enable slvs 100 Ω termination on ck6b
        self.g1_tab['slvs6b'] = (10, 4, 1)
        
        # sL0enaV,disable mixed signal funct. If L0 enabld
        self.g1_tab['sL0enaV'] = (10, 5, 1)
        
        self.g1_tab['slh'] = (10, 6, 1)
        self.g1_tab['slxh'] = (10, 7, 1)
        self.g1_tab['stgc'] = (11, 0, 1)
        
        # reset1 and reset2, Hard reset when both high
        self.g1_tab['reset'] = (11, 6, 2)
        
        
        ###########################################
        #global register 2
        ###########################################
        self.g2_tab = {}
        
        # nskipm_i, sets magic number on BCID  0xFE8
        self.g2_tab['nskipm_i'] = (3, 7, 1)
        
        # sL0cktest,  enable clocks when L0 core disabled (test)
        self.g2_tab['sL0cktest'] = (4, 0, 1)
        
        # sL0dckinv, invert DCK
        self.g2_tab['sL0dckinv'] = (4, 1, 1)
        
        # sL0ckinv,  invert BCCLK
        self.g2_tab['sL0ckinv'] = (4, 2, 1)
        
        # sL0ena,enable L0 core/reset core & gate clk if 0
        self.g2_tab['sL0ena'] = (4, 3, 1)
        
        #  truncate i0:5 ,  Max hits per L0
        self.g2_tab['truncate_i'] = (4, 4, 6)
        
        # nskip i0:6,
        self.g2_tab['nskip_i'] = (5, 2, 7)
        
        #  window i0:2, Size of trigger window
        self.g2_tab['window_i'] = (6, 1, 3)
        
        # rolloveri0:11, channel tagging BC rollover
        self.g2_tab['rollover_i'] = (6, 4, 12)
        
        # l0oﬀset i0:11 , L0 BC offset
        self.g2_tab['l0offset_i'] = (8, 0, 12)
        
        # oﬀset i0:11 , Channel tagging BC oﬀset
        self.g2_tab['offset_i'] = (9, 4, 12)

    def mod(self, name, val, channel=-1, reverse=True):
        if channel >= 0 and channel < 64:
            if name in self.ch_tab:
                aux = self.ch_tab[name]
                idx = 12+3*channel+aux[0]
            else:
                # print("No matching name for channel registers!")
                # return
                raise KeyError("No matching name for channel registers!")
        else:
            if name in self.g1_tab:
                aux = self.g1_tab[name]
                idx = aux[0]
                if reverse == True and name in ('sm', 'st', 'sg', 'stc', 'sdt', 'sdp10'):
                    rs = ''.join(reversed(bin(val)[2:]))
                    val = int(rs + (aux[2] - len(rs)) * '0', 2)
            elif name in self.g2_tab:
                aux = self.g2_tab[name]
                idx = 204 + aux[0]
            else:
                # print("No matching name for global registers!")
                # return
                raise KeyError("No matching name for global registers!")

        if val > 2**aux[2]:
            raise ValueError("Entered value for this config item is too large!")

        if aux[1] + aux[2] > 8:
            rest = aux[2] - (8 - aux[1])
            h = (1 << (aux[2] - rest)) - 1

            self.cfg_arr[idx] = self.cfg_arr[idx] & (255 ^ (h << aux[1]))
            # print("{} {} {}".format(val, h, aux[1]))
            self.cfg_arr[idx] = self.cfg_arr[idx] | ((val & h) << aux[1])
            inc = 1

            while rest > 8:
                self.cfg_arr[idx+inc] = (val >> (aux[2] - rest)) & 255
                rest = rest - 8
                inc = inc + 1

            self.cfg_arr[idx+inc] = self.cfg_arr[idx+inc] & (255 ^ ((1 << rest) - 1))
            self.cfg_arr[idx+inc] = self.cfg_arr[idx+inc] | (val >> (aux[2] - rest))
        else:
            self.cfg_arr[idx] = self.cfg_arr[idx] & (255 ^ (((1 << aux[2]) - 1) << aux[1]))
            self.cfg_arr[idx] = self.cfg_arr[idx] | (val << aux[1])
    
    def zeroes(self):
        for i, _ in enumerate(self.cfg_arr):
            self.cfg_arr[i] = 0

    def get(self):
        return bytearray(self.cfg_arr)

    def send(self, port):
        return gui_serial.serial_write('vmm_cfg.cfg = {}'.format(self.get()), port, timeout=1)

    