"""
    vmm_temp.py

    Functions for measuring temperature    
"""

import ujson as json
import machine
import vmm_cfg
import utime
from vmm_serial import serial_write
from vmm_const import FPGA_ADDR, ADC1_ADDR, ADC2_ADDR


def readout():

    i2c = machine.I2C('Y')

    # reconfigure VMMs to output 
    vmm_cfg.send_bram(i2c, FPGA_ADDR)
    vmm_cfg.reconfigure(i2c, FPGA_ADDR)
    utime.sleep_us(100)

    ret = _measure(i2c, ADC1_ADDR) + _measure(i2c, ADC2_ADDR)
    serial_write(json.dumps(ret))

def _measure(i2c, addr):
    # i2c.writeto_mem(addr, 1, b'\xE5\x83') # commanding one measurement, for continuous
    #                                       # use E4 instead of E5
    # utime.sleep_us(100)
    # val = i2c.readfrom_mem(addr, 0, 2)
    # utime.sleep_us(100)

    # i2c.writeto_mem(addr, 1, b'\xE5\x83')
    # utime.sleep_us(100)
    # val = i2c.readfrom_mem(addr, 0, 2)


    i2c.writeto_mem(addr, 1, b'\xE4\x83')
    utime.sleep_us(800)
    val = i2c.readfrom_mem(addr, 0, 2)
    i2c.writeto_mem(addr, 1, b'\x65\x83')
    
    val_mV = int.from_bytes(val, 'big') >> 4
    temp = (725 - val_mV) / 1.85
    return (val_mV, temp)