"""
    vmm_power_cycle.py
    
    Now switches between power on and off    
"""

import utime

from vmm_serial import serial_write
from vmm_ctrl import GND_EN, CLK_EN, PWR_EN

def power_cycle():
    """ Turns the VTC on or off"""


    if PWR_EN.value() == 0:
        GND_EN.value(1)
        CLK_EN.value(1)
        utime.sleep_us(100)
        PWR_EN.value(1)
    else:
        PWR_EN.value(0)
        utime.sleep_us(500)
        CLK_EN.value(0)
        GND_EN.value(0)

    # ------------------------------------------
    serial_write("Power cycle done")

def status():
    serial_write([GND_EN.value(), CLK_EN.value(), PWR_EN.value()])