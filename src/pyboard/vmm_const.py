from micropython import const

FPGA_ADDR = const(65)
ADC1_ADDR = const(72)
ADC2_ADDR = const(73)
EEPROM_ADDR = const(80)
ID_ADDR = const(88)

AMUX_ADDR = const(32)