""" vmm_mux3.py"""

import ujson as json
import utime
from vmm_serial import serial_write
from vmm_ctrl import PWR_EN, AMUX_EN, GND_EN, CLK_EN
from vmm_const import AMUX_ADDR

def readout():
    import machine

    ret = {} # return object contains values to be transmitted from VMM to host

    PWR_EN.value(0)			# disable power P1 and P2 for VMM (green LED off)
    CLK_EN.value(0)			# disable 40 MHz clock
    GND_EN.value(0)			# disconnect HDMI pins 2,5,11,17 from GND
    AMUX_EN.value(1)		# enable analogue multiplexters for ADCs

    # --------------------------------------------------------------
    # Control Multiplexers via I2C bus No 1
    # --------------------------------------------------------------
    i2c = machine.I2C('X')		# set up I2C via X9 (SCL) and X10 (SCA)
    # from machine import Pin		# enable I2C  pullups inside MCU

    # Pin('PULL_SDA', Pin.OUT, value=1)
    # Pin('PULL_SCL', Pin.OUT, value=1)

    devices = i2c.scan()   			#scan and find i2C address of REG chip ( PCA9534 )
    addr = devices[0]			# note that -[0] is address found (= 32)

    if AMUX_ADDR != addr:
        raise AssertionError("Pyboard I2C can't find the IO expander (addr 32) on the VTC board")

    i2c.writeto_mem(addr, 3, b'\x00') 	# write to REG chip CTRLregister 3: make all 8 outputs

    # ----------------------------------------------------------------
    #  Set up ADC 2 to read y0,y1,y2..y7  line impedances
    # ----------------------------------------------------------------
    from pyb import Pin, ADC

    adc2 = ADC(Pin('Y12')) 		# read ADC2 on PYBD  Y12
    ADCbit = 0.00080566		# conversion of 1 ADC bit into volt

    # ---------- impedance Trg-Ctrl_p line ------------HDMI PIN 7 ------------------
    i2c.writeto_mem(addr, 1, b'\x00') 	# set MUX3 address to y0 pin (TrgCtrl_p)

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UtrgCtrl_p = adc2.read()*ADCbit
    RX = UtrgCtrl_p/(3.3-UtrgCtrl_p)
    ret["pin7"] = {"U": UtrgCtrl_p, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',UtrgCtrl_p,'V')
    # print('Trg/Ctrl pin 7=',round(RX,2),'MOHM')
    # TrgC/trl_p  pin 7 should be > 1 MOHM

    #
    # ---------- impedance Trg-Ctrl_n line --------------HDMI PIN 9-----------------
    #
    i2c.writeto_mem(addr, 1, b'\x08') 	# set MUX3 address to y1 pin (Trg-Ctrl_n)

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UtrgCtrl_n = adc2.read()*ADCbit
    RX = UtrgCtrl_n/(3.3-UtrgCtrl_n)
    ret["pin9"] = {"U": UtrgCtrl_n, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',UtrgCtrl_n,'V')
    # print('Trg/Ctrl_n pin 9=',round(RX,2),'MOHM')
    # Trg/Ctrl_n  pin 9 should be  > 100 kOHM

    #
    # ---------- impedance Data2_P line -----------------HDMI PIN 4-----------------
    #
    i2c.writeto_mem(addr, 1, b'\x10') # set MUX3 address to y2 pin (Data2_P)

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    Udata2_p = adc2.read()*ADCbit
    RX = Udata2_p/(3.3-Udata2_p)
    ret["pin4"] = {"U": Udata2_p, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',Udata2_p,'V')
    # print('Data2_P pin4 =',round(RX,2),'MOHM')
    # Data2_P  pin 4 should be  > 100 kOHM

    #
    # ---------- impedance Data2_N line ------------------HDMI PIN 6 ----------------
    #
    i2c.writeto_mem(addr, 1, b'\x18') # set MUX3 address to y3 pin (Data2_N)

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    Udata2_n = adc2.read()*ADCbit
    RX = Udata2_n/(3.3-Udata2_n)
    ret["pin6"] = {"U": Udata2_n, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',Udata2_n,'V')
    # print('Data2_N  pin6 =',round(RX,2),'MOHM')
    # Data2_P pin 6  should be  > 100 kOHM

    #
    # ----------  P2A Voltage test -----------( no pin)-----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x20') 	# set MUX3 address to y4 pin P2A
    UP2A = adc2.read()*ADCbit*2 	        # connects to voltage divider 0.5 P2A
    # FIXME:what todo with this value?
    # print('U2A generator =',UP2A,'V')	# P2A generator Voltage should be > 3.5V

    #
    # ----------  P1A Voltage test ------------(no pin) ----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x28') 	# set MUX3 address to y5 pin P1A
    UP1A = adc2.read()*ADCbit*2	    	# connects to voltage divider 0.5 P1A
    # FIXME:what todo with this value?
    # print('U1A generator=',UP1A,'V')	# P1A generator Voltage  should be  > 3.8V

    #
    # -----------CLK_P  line test -------------HDMI PIN 10-----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x30') 	# set MUX3 address to y6 pin CLK_P

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UCLK_p = adc2.read()*ADCbit
    RX = UCLK_p/(3.3-UCLK_p)
    ret["pin10"] = {"U": UCLK_p, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UCLK_p,'V')
    # print('CLK_p pin10 =',round(RX,2),'MOHM')
    # CLK_p  pin 10  should be  > 1 MOHM

    #
    # -----------CLK_N  line test --------------HDMI PIN 12-----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x38') 	# set MUX3 address to y7 pin CLK_N

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UCLK_n = adc2.read()*ADCbit
    RX = UCLK_n/(3.3-UCLK_n)
    ret["pin12"] = {"U": UCLK_n, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UCLK_n,'V')
    # print('CLK_n pin 12=',round(RX,2),'MOHM')
    # CLK_n   pin 12  should be  > 1 MOHM
    serial_write(json.dumps(ret))
