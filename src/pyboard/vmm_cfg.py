""" 
    vmm_cfg.py

    Functions for sending VMM configuration over I2C either to EEPROM or directly to BRAM of Spartan

    User first needs to redefine cfg variable through UART to desired configuration and then can use functions
    defined here.    
"""

import utime
import pyb

cfg = bytearray(216)

def send(i2c, addr):
    global cfg
    lim = int(len(cfg)/8)
    for inc in range(0, lim):
        i2c.writeto_mem(addr, inc*8, cfg[inc*8:inc*8+8])
        utime.sleep_ms(10)

def send_modified(i2c, addr):
    global cfg
    lim = int(len(cfg)/8)
    for inc in range(0, lim):
        prev = i2c.readfrom_mem(addr, inc*8, 8)
        diff = []
        for i, p in enumerate(prev):
            if p != cfg[inc*8+i]:
                diff.append(i)
        if len(diff) > 0:
            i2c.writeto_mem(addr, inc*8+diff[0], cfg[inc*8+diff[0]:inc*8+diff[-1]+1])
            utime.sleep_ms(10)

def send_bram(i2c, addr):
    global cfg
    i2c.writeto_mem(addr, 1280, cfg, addrsize=16)
    

def reconfigure(i2c, fpga, attempts=5, cmd=b'\x02'):
    for _ in range(attempts):
        i2c.writeto_mem(fpga, 0, cmd)
        # if check_reconfig(i2c, fpga) == True:
#             return
        aux = check_reconfig(i2c, fpga)
        if aux == (True, True):
        	return aux
    return aux
#     raise RuntimeError("Reconfiguration of VMMs wasn't successful")

def eeprom_reconfigure(i2c, eeprom, fpga, only_mod=True, attempts=5):
    if only_mod == True:
        send_modified(i2c, eeprom)
    else:
        send(i2c, eeprom)

    reconfigure(i2c, fpga, attempts, b'\x01')

def check_reconfig(i2c, fpga):
    start = pyb.millis()
    while pyb.elapsed_millis(start) < 5:
        reg = i2c.readfrom_mem(fpga, 0, 1)
        if int.from_bytes(reg, 'big') & 24 == 24:
            break
#         if pyb.elapsed_millis(start ) > 5:
#         	return False
    
    aux = int.from_bytes(reg, 'big')
    return (aux & 32 == 0 and aux & 8 == 8, aux & 64 == 0 and aux & 16 == 16)
    
#     if int.from_bytes(reg, 'big') & 96 == 0:
#         return True
#     else:
#         return False