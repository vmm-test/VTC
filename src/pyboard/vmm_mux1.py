#  REV 2 17.05.2020  Threshold level for min. GND voltage added 
#  REV 1 29.02.2020  HDMI PIN numbers added sorina, missing 8, 15, 16
#
#  MUX1 & MUX3 line test for connected VMM3a  hybrid with VTC 1.0 hardware
# -power, clock and GND lines disabled
# -measure ohmic impedance of lines data1, M/S
#
# uPython v1.11 -580-g973f68780  on  PYBD-SF6W

import ujson as json
import utime

from vmm_serial import serial_write
from vmm_ctrl import PWR_EN, AMUX_EN, GND_EN, CLK_EN
from vmm_const import AMUX_ADDR

def readout():
    import machine
    # from machine import Pin

    ret = {}    # return object contains values to be transmitted from VMM to host

    PWR_EN.value(0)			# disable power P1 and P2 for VMM (green LED off)
    utime.sleep_us(100)
    CLK_EN.value(0)			# disable 40 MHz clock
    GND_EN.value(0)			# disconnect HDMI pins 2,5,11,17 from GND
    AMUX_EN.value(1)	    # enable analogue multiplexters for ADC

    # ---------------------------------------------------------------
    # Control Multiplexers via I2C bus No 1
    # ---------------------------------------------------------------

    i2c = machine.I2C('X')		# set up I2C via X9 (SCL) and X10 (SCA)
    # from machine import Pin		# enable I2C  pullups inside MCU
    # Pin('PULL_SDA', Pin.OUT, value=1)
    # Pin('PULL_SCL', Pin.OUT, value=1)

    devices = i2c.scan()   		#scan and find i2C address of REG chip ( PCA9534 )
    addr = devices[0]			# note that -[0] is address found (= 32)

    if AMUX_ADDR != addr:
        raise AssertionError("Pyboard I2C can't find the IO expander (addr 32) on the VTC board")

    i2c.writeto_mem(addr, 3, b'\x00') 	# write to REG chip CTRLregister 3: make all 8 outputs

    # ----------------------------------------------------------------
    #  Set up ADC 1 to read y0,y1,y2  line impedances
    # ----------------------------------------------------------------

    from pyb import Pin, ADC

    adc1 = ADC(Pin('X12')) 		# read ADC1 on PYBD  X12
    ADCbit = 0.00080566			# conversion of 1 ADC bit into volt

    #
    # ---------- impedance data1_P line ----HDMI PIN 1 ---------------
    #
    i2c.writeto_mem(addr, 1, b'\x00') 	# set MUX1 address to y0 pin (data1_P)

    # let RC ( 1M*1n=1ms) on MUX  input settle
    utime.sleep_ms(100)

    Udata1p = adc1.read()*ADCbit
    RX = Udata1p/(3.3-Udata1p)

    ret["pin1"] = {"U": Udata1p, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',Udata1p,'V')
    # print('data1_p pin1 =',round(RX,2),'MOHM')
    # data1_P  Pin1   should be  > 1 MOHM


    #
    # ---------- impedance data1_N line -----HDMI PIN 3------------------
    #
    i2c.writeto_mem(addr, 1, b'\x01') 	# set MUX1 address to y1 pin (data1_N)

    # let RC ( 1M*1n=1ms) on MUX  input settle
    utime.sleep_ms(100)

    Udata1n = adc1.read()*ADCbit
    RX = Udata1n/(3.3-Udata1p)
    ret["pin3"] = {"U": Udata1n, "R": round(RX, 2), "Runits": "MOHM"}

    # print('U=',Udata1n,'V')
    # print('data1_n= pin 3',round(RX,2),'MOHM')
    # data1_N   pin 3 should be > 1 MOHM

    #
    # ---------- impedance M/S line -----------HDMI PIN 13 -----------------
    #
    i2c.writeto_mem(addr, 1, b'\x02') # set MUX1 address to y2 pin (Master/Slave line)
    
    # let RC ( 1M*1u=1s) on MUX  input settle
    utime.sleep_ms(1000)

    UMS = adc1.read()*ADCbit
    RX = 1000*UMS/(3.3-UMS)
    ret["pin13"] = {"U": UMS, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UMS,'V')
    # print('M/S= pin 13',round(RX,2),'kOHM')
    # M/S line  pin 13  should be  > 1 kOHM

    #
    # ---------- impedance P2B line test ------HDMI -PIN 14 and 19 ------------
    #
    i2c.writeto_mem(addr, 1, b'\x03') 	# set MUX1 address to y3 pin P2B
    
    # let RC ( 1M*1u=1s) on MUX  input settle
    utime.sleep_ms(1000)

    UP2B = adc1.read()*ADCbit
    RX = 1000*UP2B/(3.3-UP2B)
    ret["pin14"] = {"U": UP2B, "R": round(RX, 2), "Runits": "kOHM"}
    ret["pin19"] = {"U": UP2B, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UP2B,'V')
    # print('P2B  pins 14+19 =',round(RX,2),'kOHM')
    # P2B  pin 14 and 19 should be   > 1 kOHM

    #
    # ---------- impedance P1B line test --------HDMI pin 18 -------------------
    #
    i2c.writeto_mem(addr, 1, b'\x04') 	# set MUX3 address to y4 pin P1B
    
    # let RC ( 1M*1u=1s) on MUX  input settle
    utime.sleep_ms(1000)

    UP1B = adc1.read()*ADCbit
    RX = 1000*UP1B/(3.3-UP1B)
    ret["pin18"] = {"U": UP1B, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UP1B,'V')
    # print('P1B pin 18 =',round(RX,2),'kOHM')
    # P1B line pin 18 should be  >1 kOHM

    #
    # ---------- impedance test of test cable to AUX --------( separate cable ) ------------
    #
    i2c.writeto_mem(addr, 1, b'\x05') 	# set MUX3 address to y5 pin test input

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    Utest = adc1.read()*ADCbit
    RX = 1000000*Utest/(3.3-Utest)
    ret["aux"] = {"U": Utest, "R": round(RX, 2), "Runits": "OHM"}

    # print('U=',Utest,'V')
    # print('AUX-cable on P2=',round(RX,2),'OHM')
    # AUX test cable on P2 should be   > 1000 OHM

    #
    # ---------- check low impedance of GND lines ----------------------
    THRESH=0.0025						#lowest reasonable voltage 
    #
    # -----------GND-A  line test ------------HDMI pin 2-------------------------
    #
   
    i2c.writeto_mem(addr, 1, b'\x07') 	# set MUX1 address to y7 pin and y0 of MUX2

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UGNDA = adc1.read()*ADCbit
    if UGNDA < THRESH:
		UGNDA=0
    RX = 1000*UGNDA/(3.3-UGNDA)
    ret["pin2"] = {"U": UGNDA, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UGNDA,'V')
    # print('GND-A pin2 =',round(RX,2),'kOHM')
    # GND-A    < 5 OHM


    #
    # -----------GND-B  line test --------------HDMI pin 5-----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x47') 	# set MUX1 address to y7 pin and y1 of MUX2

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UGNDB = adc1.read()*ADCbit
    if UGNDB < THRESH:
		UGNDB=0
    RX = 1000*UGNDB/(3.3-UGNDB)
    ret["pin5"] = {"U": UGNDB, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UGNDB,'V')
    # print('GND-B pin 5=',round(RX,2),'kOHM')
    # GND-B  line pin 5  should be  < 5 OHM

    #
    # -----------GND-C  line test ---------------HDMI pin 11-----------------------
    #
    i2c.writeto_mem(addr, 1, b'\x87') 	# set MUX1 address to y7 pin and y2 of MUX2

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UGNDC = adc1.read()*ADCbit
    if UGNDC < THRESH:
		UGNDC=0
    RX = 1000*UGNDC/(3.3-UGNDC)
    ret["pin11"] = {"U": UGNDC, "R": round(RX, 2), "Runits": "kOHM"}

    # print('U=',UGNDC,'V')
    # print('GND-C pin 11=',round(RX,2),'kOHM')
    # GND-C  line pin 11 should be  < 5 OHM

    #
    # -----------GND-D  line test ----------------HDMI pin 17------------------------------
    #
    i2c.writeto_mem(addr, 1, b'\xC7') 	# set MUX1 address to y7 pin and y3 of MUX2

    # let RC ( 1M*0.1=0.1s) on MUX  input settle
    utime.sleep_ms(400)

    UGNDD = adc1.read()*ADCbit
    if UGNDD < THRESH:
		UGNDD=0
    RX = 1000*UGNDD/(3.3-UGNDD)
    ret["pin17"] = {"U": UGNDD, "R": round(RX, 2), "Runits": "OHM"}

    # print('U=',UGNDD,'V')
    # print('GND-D pin 17=',round(RX,2),'OHM')
    # GND-D  line pin 17 should be  < 5 OHM
    serial_write(json.dumps(ret))
