# quickly put together code for measuring baseline and its noise, for final version will be most likely modified
# (if included in this form at all)

import vmm_cfg
from vmm_serial import serial_write
import machine
import utime
import ujson as json
from vmm_ctrl import GND_EN, CLK_EN, PWR_EN

i2c = machine.I2C('Y')

def setup():
    fpga = 65
    adc1 = 72
    adc2 = 73

    GND_EN.value(1)
    CLK_EN.value(1)
    PWR_EN.value(1)
    utime.sleep_ms(3000)

    global i2c

    vmm_cfg.send_bram(i2c, fpga)
    vmm_cfg.reconfigure(i2c, fpga)
    utime.sleep_us(100)

    # i2c.writeto_mem(adc1, 1, b'\xE8\xE3')
    i2c.writeto_mem(adc2, 1, b'\xEA\xE3')

    # for i in range(count):
    #     ret = measure(i2c, adc1) + measure(i2c, adc2)
    #     serial_write(json.dumps(ret), False)
    # serial_write()
    

def measure(addr):
    global i2c
    val = i2c.readfrom_mem(addr, 0, 2)
    val_mV = (int.from_bytes(val, 'big') >> 4) / 8.0
    serial_write(json.dumps(val_mV))