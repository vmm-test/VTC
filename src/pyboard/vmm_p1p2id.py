""" vmm_p1p2id.py"""

import ujson as json
import utime
import machine
import pyb

from vmm_serial import serial_write
from vmm_ctrl import PWR_EN, AMUX_EN, GND_EN, CLK_EN
from vmm_const import AMUX_ADDR, FPGA_ADDR

def boot_test():
    CLK_EN.value(1)
    GND_EN.value(1)
    AMUX_EN.value(1)
    utime.sleep_us(100)
    PWR_EN.value(1)

    utime.sleep(4)  

    i2c = machine.I2C('Y')

    i2c.writeto_mem(FPGA_ADDR, 0, b'\x02')
    utime.sleep(1)

    ret = {"vmm0" : "", "vmm1" : ""}
    readbyte = int.from_bytes(i2c.readfrom_mem(FPGA_ADDR, 0, 1), 'big')

    turnoff = False

    if readbyte & 0x8:
        if readbyte & 0x20:
            ret["vmm0"] = "cfg"
            turnoff = True
        else:
            ret["vmm0"] = "ok"
    else:
        ret["vmm0"] = "boot"
        turnoff = True

    if readbyte & 0x10:
        if readbyte & 0x40:
            ret["vmm1"] = "cfg"
            turnoff = True
        else:
            ret["vmm1"] = "ok"
    else:
        ret["vmm1"] = "boot"
        turnoff = True

    if turnoff:
        PWR_EN.value(0)
        utime.sleep_us(500)
        AMUX_EN.value(0)
        CLK_EN.value(0)
        GND_EN.value(0)

    serial_write(json.dumps(ret))  

def readout():

    ret = {"errors": []}  # return object

    CLK_EN.value(1)
    #
    GND_EN.value(1)
    #
    AMUX_EN.value(1)
    #
    utime.sleep_us(100)
    PWR_EN.value(1)

    # ------------------------------------------
    # VMM boot delay after Powerup
    # ----------------------------------------
    # utime.sleep(4)
    # ------------------------------------------
    # Control Multiplexers via I2C bus No 1
    # ----------------------------------------
    
    i2c = machine.I2C('X')
    # machine.Pin('PULL_SDA', machine.Pin.OUT, value=1)
    # machine.Pin('PULL_SCL', machine.Pin.OUT, value=1)
    #
    devices = i2c.scan()

    if not AMUX_ADDR in devices:
        raise AssertionError("Pyboard I2C can't find the IO expander (addr 32) on the VTC board")
    #
    # addr = devices[0]
    addr = AMUX_ADDR
    #
    # print('REG chip found at decimal addr=',addr)
    ret["chip_address"] = addr

    # set all expander ports as outputs
    i2c.writeto_mem(addr, 3, b'\x00')


    # ADC conversion constant (ADCval -> voltage)
    CONV = 3.3/4096

    # set up both ADCs
    adc1 = pyb.ADC(pyb.Pin('X12'))
    adc2 = pyb.ADC(pyb.Pin('Y12'))

    # -----------------------------------------
    #  Set up and test:  ADC 2 to read P2a voltage
    # ------------------------------------------
    # i2c.writeto_mem(addr, 1, b'\x20')
    
    # -------------------------------------------
    #  Test read P2A  voltage via ADC2:
    # -------------------------------------------
    # adc2.read()
    # --------------------------------------------
    # Test and read  P2b voltage via ADC2:
    # --------------------------------------------
    # i2c.writeto_mem(addr, 1, b'\x03')
    
    # --------------------------------------------
    # CONV = const(0.00080566*2)
    # CONV * adc2.read()      # TODO: review
    # ---------------------------------------------
    # Calculate the P2 Current [A] and Voltages P2a ->P2b  over R2 (0.22 OHM)
    # --------------------------------------------
    # i2c.writeto_mem(addr, 1, b'\x00')
    # utime.sleep_us(10)
    i2c.writeto_mem(addr, 1, b'\x20') # route pin y4 (  0.5*P2a ) of MUX3 to ADC2
    # utime.sleep_us(10)
    utime.sleep_us(50000)
    # A = CONV * adc2.read() * 2
    A = CONV * _adc_meas(adc2) * 2
    
    # i2c.writeto_mem(addr, 1, b'\x00')
    # utime.sleep_us(10)
    i2c.writeto_mem(addr, 1, b'\x03') # route pin y3  (0.5* P2B) of MUX1 to ADC1    
    # utime.sleep_us(10)
    utime.sleep_us(50000)
    # B = CONV * adc1.read() * 2
    B = CONV * _adc_meas(adc1) * 2

    I2 = (A - B) / 0.22

    ret["p2"] = round(I2, 2)
    ret["p2a"] = round(A, 2)  # Volt
    ret["p2b"] = round(B, 2)  # Volt

    # -----------------------------------------------
    # Calculate P1 Current [A] and Voltages P1a -> P1b over R1 (3.3 OHM)
    # ---------------------------------------------
    i2c.writeto_mem(addr, 1, b'\x28') # set MUX3 address to 1/2 P1a
    utime.sleep_us(50000)
    C = CONV * adc2.read() * 2

    i2c.writeto_mem(addr, 1, b'\x04') # set MUX3 address to 1/2 P1b
    utime.sleep_us(50000)
    D = CONV * adc1.read() * 2

    I1 = (C - D) / 3.3

    ret["p1"] = round(I1, 2)
    ret["p1a"] = round(C, 2)  # Volt
    ret["p1b"] = round(D, 2)  # Volt

    # ------------------------------------------------
    # Measure P2 at VMM AUX connector
    # ------------------------------------------------
    i2c.writeto_mem(addr, 1, b'\x05') # route pin y5  (P2 sense wire) of MUX1 to ADC1
    utime.sleep_us(50000)
    # adc1 = ADC(Pin('X12'))
    # --------------------------------------------
    # CONV1 = const(0.00080566)
    P2 = (CONV * adc1.read()) - 0.05

    ret["p2vmm"] = round(P2, 2)


    # ------------------------------------------------
	# Measure P1 at VMM AUX connector
	# ------------------------------------------------		
    i2c.writeto_mem(addr, 1, b'\x06') 	# route pin y6  (P1 sense wire) of MUX1 to ADC1
    utime.sleep_us(50000)
    # adc1 = ADC(Pin('X12')) 		# read ADC2 on pin X12 
        # --------------------------------------------
    # CONV1=0.00080566 		# conversion of ADC  bits in Volt	
    P1=( CONV * adc1.read() ) - 0.05 # correct for 0.05 V GND dropoff over cable	 	
    # print('P1 at VMM=',round(P1,2),'Volt')		# P1 should be between 2.9 and 3.3V
    ret["p1vmm"] = round(P1, 2)

    # -------
    # scan of I2C and chip ID
    # ---------------------------------

    # switch to second I2C that's connected to the HDMI
    i2c = machine.I2C('Y')

    r = i2c.scan()
    ret["uadc1"] = None
    ret["uadc2"] = None
    ret["eeprom"] = None
    ret["id_chip"] = None # AT24CS02_ID
    ret["fpga"] = 0 # need to test on the old hybrid version, for future it should be None
    try:
      for idx in r:
        if idx==72:
            ret["uadc1"] = idx
        elif idx==73:
            ret["uadc2"] = idx
        elif idx==80:
            ret["eeprom"] = idx
        elif idx==88:
            ret["id_chip"] = idx
        elif idx==65:
            ret["fpga"] = idx
    except:
        ret["errors"].append("I2C: failed to read uadc1,2,e eeprom, chip number")
    try:
    	# i2c.writeto(88, b'\x80')
    	# U = i2c.readfrom(88, 16)
        U = i2c.readfrom_mem(88, 0x80, 16)
    	chip_id = ["{:x}".format(U[i]) for i in range(16)]
    	ret["chip_id"] = "".join(chip_id)
    except:
    	ret["chip_id"] = None
    	ret["errors"].append("I2C: failed to read CHIP ID")

    serial_write(json.dumps(ret))

def _adc_meas(adc, count=10, us_wait=1000):
    sum = 0
    for i in range(count):
        sum += adc.read()
        utime.sleep_us(us_wait)
    return sum/count
