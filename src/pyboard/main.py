# main.py -- put your code here!

# TO BE DECIDED
# we can put imports here, then we won't have to import them through import_module called from gui_serial
# (we would lose ability to refresh the modules, but that could be facilitated through reset of pyboard)
import vmm_const
import vmm_cfg
import vmm_temp
import vmm_baseline
import vmm_ctrl
from vmm_serial import serial_write

machine.Pin('PULL_SDA', machine.Pin.OUT, value=1)
machine.Pin('PULL_SCL', machine.Pin.OUT, value=1)

def vmm_request():
    serial_write("vmm_reply")

import array
import utime

# Doro: I think that this is not used, ask Marek to remove
def temp_meas(vmm):
    if vmm == 1:
        addr = 72
    else:
        addr = 73

    i2c.writeto_mem(addr, 1, b'\xE5\x83') # commanding one measurement, for continuous
                                          # use E4 instead of E5
    utime.sleep_us(100)
    val = i2c.readfrom_mem(addr, 0, 2)
    utime.sleep_us(100)

    i2c.writeto_mem(addr, 1, b'\xE5\x83')
    utime.sleep_us(100)
    val = i2c.readfrom_mem(addr, 0, 2)
    
    val_mV = int.from_bytes(val, 'big') >> 4
    # print(val_mV)
    temp = (725 - val_mV) / 1.85
    print("Measured voltage at MO pin: {} mV".format(val_mV))
    print("Temperature of VMM{}: {} °C".format(vmm, temp))
    

