""" vmm_serial.py"""

def serial_write(command, end=True):
    """ Send command to host computer

        command : any
            stuff to write

        end : boolean
            should finalize communication with host and send transaction terminator
    """

    print(command)
    if end:
        print("0xDEADBEEF")
