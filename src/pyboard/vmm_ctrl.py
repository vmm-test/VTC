""" vmm_ctrl.py 

    Holds settings of the pins - can be used to preserve their state across all pyboard functions
    
    In the end should be used in all pyboard modules
"""

from machine import Pin

PWR_EN = Pin('X3', Pin.OUT)
AMUX_EN = Pin('X1', Pin.OUT)
GND_EN = Pin('X6', Pin.OUT)
CLK_EN = Pin('X4', Pin.OUT)

GND_EN.value(0)
CLK_EN.value(0)
AMUX_EN.value(0)
PWR_EN.value(0)