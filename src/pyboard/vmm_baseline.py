# quickly put together code for measuring baseline and its noise, for final version will be most likely modified
# (if included in this form at all)

import vmm_cfg
from vmm_serial import serial_write
import machine
import utime
import ujson as json
# from vmm_ctrl import GND_EN, CLK_EN, PWR_EN
from vmm_const import FPGA_ADDR, ADC1_ADDR, ADC2_ADDR

i2c = machine.I2C('Y')

adjust = [0, 0]

def setup(vmms):

    # manipulating these pins should not be necessary to do here (including the wait)
    # GND_EN.value(1)
    # CLK_EN.value(1)
    # PWR_EN.value(1)
    # utime.sleep_ms(2000)

    global i2c

    vmm_cfg.send_bram(i2c, FPGA_ADDR)
    vmm_cfg.reconfigure(i2c, FPGA_ADDR)
    utime.sleep_us(100)

    # this could be switchable so during consecutive measurements there is no need to send same commands to ADCs
    if vmms == 1 or vmms == 3:
        i2c.writeto_mem(ADC1_ADDR, 1, b'\xEA\xE3')
    if vmms > 1:
        i2c.writeto_mem(ADC2_ADDR, 1, b'\xEA\xE3')

def adjust_range(high_range, vmms):
    if high_range != 0:
        data = b'\xE4\xE3'
    else:
        data = b'\xEA\xE3'

    global adjust
    if vmms == 1 or vmms == 3:
        i2c.writeto_mem(ADC1_ADDR, 1, data)
        adjust[0] = high_range
    if vmms > 1:
        i2c.writeto_mem(ADC2_ADDR, 1, data)
        adjust[1] = high_range

def change_channel():
    global i2c
    i2c.writeto_mem(FPGA_ADDR, 5, b'\x00'+vmm_cfg.cfg[0:2])
    vmm_cfg.reconfigure(i2c, FPGA_ADDR)

def measure(vmms):
    global i2c
    global adjust
    if vmms == 1 or vmms == 3:
        val = i2c.readfrom_mem(ADC1_ADDR, 0, 2)
        # val_mV1 = (int.from_bytes(val, 'big') >> 4) / 8.0
        val_mV1 = int.from_bytes(val, 'big') >> 4
        if adjust[0] == 0:
            val_mV1 = val_mV1 / 8.0
    else:
        val_mV1 = 0

    if vmms > 1:
        val = i2c.readfrom_mem(ADC2_ADDR, 0, 2)
        # val_mV2 = (int.from_bytes(val, 'big') >> 4) / 8.0
        val_mV2 = int.from_bytes(val, 'big') >> 4
        if adjust[1] == 0:
            val_mV2 = val_mV2 / 8.0
    else:
        val_mV2 = 0
    
    serial_write(json.dumps((val_mV1, val_mV2)))

def finish(vmms):
    if vmms == 1 or vmms == 3:
        i2c.writeto_mem(ADC1_ADDR, 1, b'\x6B\xE3')
    if vmms > 1:
        i2c.writeto_mem(ADC2_ADDR, 1, b'\x6B\xE3')

